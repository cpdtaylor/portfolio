//
//  IngredientsViewController.swift
//  NatureBake
//
//  Created by Chris Taylor on 14/04/2016.
//  Copyright © 2016 naturebake. All rights reserved.
//  Chris Taylor <c.p.d.taylor@ncl.ac.uk>
//  Student Number: 120121386

import UIKit


/**
 A View Controler for the ingredient screen
 */
class IngredientsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    
    //Storage data - set by seg controller in RecipeViewController
    var ingredientsCollection = [IngredientModel]()
    var allergens = Set<String>()
    
    //Set by seg controller in RecipeViewController
    var recipename : String = ""
    var image : UIImage = UIImage()
    
    //Displayed information
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var recipeNameLabel: UILabel!
    @IBOutlet weak var bakeImage: UIImageView!
    @IBOutlet weak var allergensLabel: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.listTableView.delegate = self
        self.listTableView.dataSource = self
        
        // Set allergens label
        var allergenString = ""
        for allergen in allergens
        {
            allergenString += allergen + "  "
        }
        
        self.allergensLabel.text = allergenString
        self.recipeNameLabel.text = recipename
        self.bakeImage.image = image

    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.ingredientsCollection.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        // Retrieve cell
        let cellIdentifier: String = "IngredientCell"
        let myCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        let item: IngredientModel = self.ingredientsCollection[indexPath.row]
        
        //Make attributed text for ingredient cell
        myCell!.textLabel!.attributedText = makeAttributedCellText(item.container!, subtitle: item.name!)
        
        return myCell!
        
    }
    
    //Automatic cell dimentions
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    //Automatic cell dimentions
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    
/**
 Format text for an ingredient cell
 - parameter title: Container for ingredient
 - parameter subtitle: name of ingredient
 - return : Attributed text for cell
 */
    func makeAttributedCellText(title: String, subtitle: String) -> NSAttributedString
    {
        
        let titleAttributes = [NSFontAttributeName: UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline), NSForegroundColorAttributeName: UIColor(red:0.89, green:0.71, blue:0.78, alpha:1.0)]
        let subtitleAttributes = [NSFontAttributeName: UIFont.preferredFontForTextStyle(UIFontTextStyleSubheadline)]
        
        let titleString = NSMutableAttributedString(string: "\(title)\n", attributes: titleAttributes)
        let subtitleString = NSAttributedString(string: subtitle, attributes: subtitleAttributes)
        
        titleString.appendAttributedString(subtitleString)
        
        return titleString
    }
}