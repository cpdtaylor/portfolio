//
//  RecipeViewController.swift
//  NatureBake
//
//  Created by Chris Taylor on 27/02/2016.
//  Copyright © 2016 naturebake. All rights reserved.
//  Chris Taylor <c.p.d.taylor@ncl.ac.uk>
//  Student Number: 120121386

import UIKit
import CoreData
import AVFoundation


/**
A View Controller for the Recipe screen
*/
class RecipeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, AVSpeechSynthesizerDelegate
{
    
    //Storage
    var instructionCollection = [InstructionModel]()
    var ingredientCollection = [IngredientModel]()
    var allergens = Set<String>()
    
    //Displayed Information
    @IBOutlet weak var recipeNameLabel: UILabel!
    @IBOutlet weak var servingLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var cookingTimeLabel: UILabel!
    @IBOutlet weak var bakeImage: UIImageView!
    @IBOutlet weak var listTableView: UITableView!

    //JSON data service
    var service: Service!
    
    //User entered code
    var code = ""
    
    //Default Youtube link
    var youtubelink: String = "www.youtube.com/naturebake"
    
    //Speech
    let synthesizer = AVSpeechSynthesizer()
    @IBOutlet weak var speechButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    
    
    
/**
Event for youtube button. If the youtube button is pressed then the user is redirected to a youtube video demonstration for the recipe
- parameter sender: youtubeButton     
*/
    @IBAction func youtubeButtonPressed(sender: UIButton)
    {
        let url = NSURL(string:self.youtubelink)!
        if UIApplication.sharedApplication().canOpenURL(url)
        {
            //Open safari
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
/**
Action for when speechButton is pressed. Instructions are read out
 - parameter sender: speechButton
 */
    @IBAction func speechAction(sender: UIButton)
    {
        if !self.synthesizer.speaking
        {
            var s = ""
            var step = 1
            
            //Collect all instructions into a string
            for instruction in instructionCollection
            {
                s += "Step \(step). " + instruction.des! + ". "
                step += 1
            }
            
            let utterance = AVSpeechUtterance(string: s)
            //Currently the best sounding voice
            utterance.voice = AVSpeechSynthesisVoice(language: "en-AU")
            //Utterance should not be to fast
            utterance.rate = 0.45
            self.synthesizer.speakUtterance(utterance)

        }
        else
        {
            //Dont interput or continue where left off
            self.synthesizer.continueSpeaking()
        }
        
        //Hide speech button
        animateActionButtonAppearance(true)
    }
    
/**
 Action for when pauseButton is pressed. Speech is paused
 - parameter sender: pauseButton
 */
    @IBAction func pauseSpeech(sender: AnyObject)
    {
        //Pause immediately - seems more responsive
        self.synthesizer.pauseSpeakingAtBoundary(AVSpeechBoundary.Immediate)
        
        //Hide pause button
        animateActionButtonAppearance(false)
    }

/**
 Hide/display speech functionality buttons
 - parameter shouldHideSpeakButton: true to hide speak button
 */
    func animateActionButtonAppearance(shouldHideSpeakButton: Bool)
    {
        
        UIView.animateWithDuration(0.25, animations: { () -> Void in
            if shouldHideSpeakButton
            {
                self.speechButton.alpha = 0.0
                self.speechButton.layer.zPosition = 0;
                self.pauseButton.layer.zPosition = 100;
                self.pauseButton.alpha = 1.0;
            }
            else
            {
                self.speechButton.alpha = 1.0
                self.speechButton.layer.zPosition = 100;
                self.pauseButton.layer.zPosition = 0;
                self.pauseButton.alpha = 0.0;
            }
            
        })
    }
  
    
/**
 When text has been read, hide pause button and show speech button
 */
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didFinishSpeechUtterance utterance: AVSpeechUtterance)
    {
        animateActionButtonAppearance(false)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.listTableView.delegate = self
        self.listTableView.dataSource = self
        
        service = Service()
        //text-to-speech service
        synthesizer.delegate = self

        //Get recipe from the entered recipe code
        service.getRecipe(code)
        {
            (response) in
            self.loadRecipe(response)
        }
        
    }
    
    

/**
Load all data from the recipe object passed from Service
*/
    func loadRecipe(recipe:NSDictionary)
    {
        if recipe["status"] as! String == "FOUND"
        {
            //Get recipe information
            let name = recipe["recipe"]!["name"] as! String
            let serving = recipe["recipe"]!["serving"] as! String
            let calories = recipe["recipe"]!["calories"] as! String
            let cookingtime = recipe["recipe"]!["cookingtime"] as! String
            let youtubelink = recipe["recipe"]!["youtube"] as! String
            let image = recipe["recipe"]!["image"] as! String
            
            //Get all instructions
            var inst: NSDictionary = NSDictionary()
            for instruction in recipe["instructions"] as! NSArray
            {
                inst = instruction as! NSDictionary
                let id = inst["id"] as! String
                let title = inst["title"] as! String
                let description = inst["description"] as! String
                
                instructionCollection.append(InstructionModel(id: id, title: title, description: description))
            }
            
            // Get all ingredients
            var ing: NSDictionary = NSDictionary()
            for ingredient in recipe["ingredients"] as! NSArray
            {
                var allergen = false
                ing = ingredient as! NSDictionary
                let id = ing["id"] as! String
                let name = ing["name"] as! String
                let container = ing["container"] as! String
                let a = ing["allergens"] as! String
                
                if a != ""
                {
                    allergen = true
                    allergens.insert(a)
                }
                ingredientCollection.append(IngredientModel(id: id, name: name, container: container, allergen: allergen))
            }
            
            dispatch_async(dispatch_get_main_queue())
            {
                self.listTableView.reloadData()
                self.recipeNameLabel.text = name
                self.servingLabel.text = "Serves \(serving)"
                self.caloriesLabel.text = "\(calories) Calories"
                self.cookingTimeLabel.text = "\(cookingtime) Mins"
                self.youtubelink = youtubelink
                //base64 string to NSData
                let decodedData = NSData(base64EncodedString: image, options: NSDataBase64DecodingOptions(rawValue: 0))
                //NSData to UIImage
                //Get the image of the bake
                self.bakeImage.image = UIImage(data: decodedData!)
            }
            
            //Code successful so save
            saveCode(self.code)
        }
        else
        {
            //Load default recipe
            self.code = "default-bake"
            service.getDefaultRecipe()
            {
                (response) in
                self.loadRecipe(response)
            }
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.instructionCollection.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        // Retrieve cell
        let cellIdentifier: String = "InstructionCell"
        let myCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        let item: InstructionModel = self.instructionCollection[indexPath.row]
        //Make formated text for cell
        myCell!.textLabel!.attributedText = makeAttributedString("\(indexPath.row + 1)", title: item.title!, subtitle: item.des!)

        return myCell!
    }
    
    //Dynamic cell dimentions
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    //Dynamic cell dimentions
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
/**
 Format text for an instruction cell
 - parameter step: The step number for the instruction
 - parameter title: Short title for instruction
 - parameter subtitle: description for instruction
 - return : Attributed text for cell
 */
    func makeAttributedString(step: String, title: String, subtitle: String) -> NSAttributedString
    {
        let titleAttributes = [NSFontAttributeName: UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline), NSForegroundColorAttributeName: UIColor(red:0.89, green:0.71, blue:0.78, alpha:1.0)]
        let subtitleAttributes = [NSFontAttributeName: UIFont.preferredFontForTextStyle(UIFontTextStyleSubheadline)]
        
        let stepString = NSMutableAttributedString(string: "\(step): ", attributes: titleAttributes)
        let titleString = NSMutableAttributedString(string: "\(title)\n", attributes: titleAttributes)
        let subtitleString = NSAttributedString(string: subtitle, attributes: subtitleAttributes)
        
        stepString.appendAttributedString(titleString)
        stepString.appendAttributedString(subtitleString)
        
        return stepString
    }

    
/**
 Set all information required by the ingredientsViewController - including allergen and ingredient information
 */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "ingredientsButton"
        {
            if let ingredientsViewController = segue.destinationViewController as? IngredientsViewController
            {
                ingredientsViewController.allergens = self.allergens
                ingredientsViewController.ingredientsCollection = self.ingredientCollection
                ingredientsViewController.image = self.bakeImage.image!
                ingredientsViewController.recipename = self.recipeNameLabel.text!
            }
        }
    }
    
    
/**
 Save the successful code to the users core data
 - parameter code: RecipeCode
*/
    func saveCode(code: String)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        //Grab RecipeCode entity
        let entity =  NSEntityDescription.entityForName("RecipeCode",
            inManagedObjectContext:managedContext)
        
        let recipecode = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext: managedContext)
        //Key is code
        recipecode.setValue(code, forKey: "code")
        // Try and save to Core Data
        do
        {
            try managedContext.save()
        }
        catch let error as NSError
        {
            print("Failed to save \(error), \(error.userInfo)")
        }
    }
    

}
