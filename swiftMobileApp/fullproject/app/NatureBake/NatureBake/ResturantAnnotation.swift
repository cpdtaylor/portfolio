//
//  ResturantAnnotations.swift
//  NatureBake
//
//  Created by Chris Taylor on 25/02/2016.
//  Copyright © 2016 naturebake. All rights reserved.
//  Chris Taylor <c.p.d.taylor@ncl.ac.uk>
//  Student Number: 120121386

import UIKit
import MapKit

/**
Resturant types
*/
enum ResturantType: Int
{
    case Vegan
    case DairyFree
}

/**
Class to store custom resturant annotations.
 
 - parameter coordinate: Coordiante for annotation.
 - parameter title: The title of the annotation
 - parameter subtitle: Extra information for the annotation.
 - parameter type: The type of the resturant
 
*/
class ResturantAnnotation: NSObject, MKAnnotation
{
    
    //Coordiant of annotation required
    var coordinate: CLLocationCoordinate2D
    //Provide a title for the annotation
    var title: String?
    //provide a subtitle (containing extra information on the resturant)
    var subtitle: String?
    
    // Type of the resturant (for example vegan or diaryfree, extendable)
    // Custom field that allowed the annotation view to change annotation image based on this value
    var type: ResturantType
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String, type: ResturantType)
    {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        self.type = type
    }
}