//
//  Service.swift
//  NatureBake
//
//  Created by Chris Taylor on 27/02/2016.
//  Copyright © 2016 naturebake. All rights reserved.
//  Chris Taylor <c.p.d.taylor@ncl.ac.uk>
//  Student Number: 120121386
//

import Foundation


/**
 Handle JSON for Naturebake recipe and ingredient queries.
*/
class Service
{
    var rest: Rest!
    
    init()
    {
        self.rest = Rest()
    }
    
/**
Get all recipes
*/
    func getRecipes(callback:(NSArray)->())
    {
        requests(rest.viewRecipes,callback: callback)
    }
/**
Get a recipe from a recipe code
*/
    func getRecipe(code: String,callback:(NSDictionary)->())
    {
        request(rest.getRecipe + code,callback: callback)
    }
    
/**
 Get default recipe
 */
    func getDefaultRecipe(callback:(NSDictionary)->())
    {
        request(rest.getDefaultRecipe,callback: callback)
    }

/**
Handle an array of JSON objects
*/
    func requests(url:String,callback:(NSArray)->())
    {
        let nsURL = NSURL(string: url)
        let task = NSURLSession.sharedSession().dataTaskWithURL(nsURL!)
        {
            (data,response,error) in do
            {
                let response = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSArray
                callback(response)
            }
            catch
            {
                print(error)
            }
        }
        task.resume()
    }

/**
 Handle a JSON object
 */
    func request(url:String,callback:(NSDictionary)->())
    {
        let nsURL = NSURL(string: url)
        let task = NSURLSession.sharedSession().dataTaskWithURL(nsURL!)
        {
            (data,response,error) in do
            {
                let response = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                callback(response)
            }
            catch
            {
                print(error)
            }
        }
        task.resume()
    }
    
}

