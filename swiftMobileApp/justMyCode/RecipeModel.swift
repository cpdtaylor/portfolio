//
//  RecipeModel.swift
//  NatureBake
//
//  Created by Chris Taylor on 27/02/2016.
//  Copyright © 2016 naturebake. All rights reserved.
//  Chris Taylor <c.p.d.taylor@ncl.ac.uk>
//  Student Number: 120121386

import Foundation

/**
 Class containing properties of a Recipe.
 
 - parameter id: database record id
 - parameter code: Naturebake Code
 - parameter name: Name of recipe
 - parameter serving: Serving number
 - parameter calories: Calorie Count
 - parameter cookingtime: Time for cooking in minutes
 - parameter youtubelink: Link (if avaliable) to online demonstration for recipe
 - parameter date: Date recipe was published
 
 */
class RecipeModel: NSObject
{
    var id : String?
    var code: String?
    var name: String?
    var serving: String?
    var calories: String?
    var cookingtime: String?
    var youtubelink: String?
    var date: String?
    
    init(id:String, code: String, name: String, serving: String, calories: String, cookingtime: String, youtubelink: String, date: String)
    {
        
        self.id = id
        self.code = code
        self.name = name
        self.serving = serving
        self.calories = calories
        self.cookingtime = cookingtime
        self.youtubelink = youtubelink
        self.date = date
        
    }

    // Return the description of a recipe
    override var description: String
    {
        return "Name: \(name), Code: \(code), Serving: \(serving), Calories: \(calories), Cooking Time: \(cookingtime), Youtube Link: \(youtubelink), Date: \(date)"
    }
    
    
}