//
//  ResturantAnnotationsView.swift
//  NatureBake
//
//  Created by Chris Taylor on 25/02/2016.
//  Copyright © 2016 naturebake. All rights reserved.
//  Chris Taylor <c.p.d.taylor@ncl.ac.uk>
//  Student Number: 120121386

import UIKit
import MapKit


/**
Resturant annotation view that extends from MKAnnotationView to allow custom annotations.
*/
class ResturantAnnotationView: MKAnnotationView
{
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
/**
Initiates MKAnnotationView constructor to access annotation property 
     and changes it to of type ResturantAnnotation and 
     then changes the image of the annotation based on its type (currently either vegan or dairyfree)
*/
    init(annotation: MKAnnotation, reuseIdentifier: String, dummy: String)
    {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        let resturantAnnotation = self.annotation as! ResturantAnnotation
        switch (resturantAnnotation.type)
        {
        case .DairyFree:
            //Set the ICON to dairyfree image
            self.image = UIImage(named: "dairyfree")
        case .Vegan:
            //Set the ICON to vegan image
            self.image = UIImage(named: "vegan")
        }
    }
}