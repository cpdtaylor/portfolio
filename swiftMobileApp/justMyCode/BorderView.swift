//
//  BorderView.swift
//  NatureBake
//
//  Created by Chris Taylor on 26/02/2016.
//  Copyright © 2016 naturebake. All rights reserved.
//  Chris Taylor <c.p.d.taylor@ncl.ac.uk>
//  Student Number: 120121386

import UIKit

/**
Defines a border for the Youtube and Ingredients buttons
*/
@IBDesignable
class BorderView: UIView
{

    @IBInspectable var cornerRadius: CGFloat = 0
    {
        didSet
        {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0
    {
        didSet
        {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor?
    {
        didSet
        {
            layer.borderColor = borderColor?.CGColor
        }
    }
    
}
