//
//  History.swift
//  NatureBake
//
//  Created by Chris Taylor on 19/04/2016.
//  Copyright © 2016 naturebake. All rights reserved.
//  Chris Taylor <c.p.d.taylor@ncl.ac.uk>
//  Student Number: 120121386

import UIKit
import CoreData


/**
A view controller for the history screen. Previou successfully submitted recipecodes are stored in core data and retrieved here 
*/
class History: UITableViewController
{

    var codes = [NSManagedObject]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.codes.count
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("HistoryCell")
        let code = codes[indexPath.row]
        cell!.textLabel!.text = code.valueForKey("code") as? String
        return cell!
    }
 
/**
Fetch all the recipe codes stored in the users data
*/
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        //Get RecipeCode entity
        let fetchRequest = NSFetchRequest(entityName: "RecipeCode")
        
        do
        {
            //Fetch the codes
            let results = try managedContext.executeFetchRequest(fetchRequest)
            self.codes = results as! [NSManagedObject]
        }
        catch let error as NSError
        {
            print("Failed to fetch \(error), \(error.userInfo)")
        }
    }

/**
If a cell is clicked - take the user to the corresponding recipe view
*/
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "HistoryCellSelection"
        {
            if let destination = segue.destinationViewController as? RecipeViewController
            {
                if let index = tableView.indexPathForSelectedRow?.row
                {
                    destination.code = codes[index].valueForKey("code") as! String
                }
            }
        }
    }



}
