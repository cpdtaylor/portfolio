<?php
/** A RESTFul API to access various data models needed for the NatureBake App
 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
 * Student Number: 120121386
 */

    use Phalcon\Loader;
    use Phalcon\Mvc\Micro;
    use Phalcon\Di\FactoryDefault;
    use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;
    use Phalcon\Http\Response;

    // Use Loader() to autoload our model
    $loader = new Loader();

    //Load all models
    $loader->registerDirs
    (
        array
        (
            __DIR__ . '/models/'
        )
    )->register();

    $di = new FactoryDefault();

    // Set up the database service
    $di->set('db', function ()
    {
        return new PdoMysql
        (
            array
            (
                "host"     => "localhost", // Change to NatureBake URL once production ready
                "username" => "app", //Test username - Change for production
                "password" => "app", //Test password - Change for production
                "dbname"   => "NatureBakeApp"
            )
        );
    });

    $app = new Micro($di);


    /**
     * @var string          Path to bake images
     */
    $imagepath = "images/";

    /**
     * @var string          Indicates that record(s) have been found
     */
    $found = "FOUND";

    /**
     * @var string          Indicates that record(s) have NOT been found
     */
    $notfound = "NOT-FOUND";

    /**
     * @var string          A phql query for receiving a recipe and its associated instructions by recipe code
     */
    $reinstphql = "SELECT R.*,I.*
        FROM Recipes R, Step S, Instructions I
        WHERE S.recipe = R.recipe_ID
        AND S.instruction_ID = I.instruction_ID
        AND R.code LIKE :code:
        ORDER BY S.step_NO";

    /**
     * @var string          A phql query for receiving the ingredients for a recipe by recipe code
     */
    $ingphql = "SELECT Ing.*
        FROM Recipes R, Step S, Instructions I, Ingredients Ing, Instructioningredient Ining
        WHERE S.recipe = R.recipe_ID
        AND S.instruction_ID = I.instruction_ID
        AND Ining.instruction_ID = I.instruction_ID
        AND Ining.ingredient_ID = Ing.ingredient_ID
        AND R.code LIKE :code:
        ORDER BY S.step_NO";


/************** Recipes ********************/

/**
 * Get all recipes
 */
    $app->get('/api/recipes', function () use ($app)
    {
        // https://docs.phalconphp.com/en/latest/reference/phql.html handles sql injections
        //Select all recipes
        $phql = "SELECT * FROM Recipes";
        $recipes = $app->modelsManager->executeQuery($phql);
        $data = array();
        foreach ($recipes as $recipe)
        {
            $data[] = recipedata($recipe);
        }
        return getresponse(recipedata($data));
    });


/**
 * Get recipe with specific code (a unique name for a recipe)
 */
    $app->get('/api/recipes/{code}', function ($code) use ($app)
    {

        //Select the recipe with the specified code
        $phql = "SELECT * FROM Recipes WHERE code LIKE :code:";
        $recipe = $app->modelsManager->executeQuery
        (
            $phql,
            array('code' => '%' . $code . '%')
        )->getFirst();

        return getresponse(recipedata($recipe));

    });

/**
 * Get recipe by its primary key id
 */
    $app->get('/api/recipes/{id:[0-9]+}', function ($id) use ($app)
    {

        //Select all recipes with the specified ID
        $phql = "SELECT * FROM Recipes WHERE recipe_ID = :id:";
        $recipe = $app->modelsManager->executeQuery
        (
            $phql,
            array('id' => $id)
        )->getFirst();

        return getresponse(recipedata($recipe));
    });


/************** ingredients ********************/

/**
 * Get all ingredients
 */
    $app->get('/api/ingredients', function () use ($app)
    {
        //Select every ingreident
        $phql = "SELECT * FROM Ingredients";
        $ings = $app->modelsManager->executeQuery($phql);
        $data = array();
        foreach ($ings as $ing)
        {
            $data[] = ingdata($ing);
        }
        return getresponse($data);
    });


/**
 * Get ingredient by its primary key id
 */
    $app->get('/api/ingredients/{id:[0-9]+}', function ($id) use ($app)
    {
        //Select all ingredients with specified ID
        $phql = "SELECT * FROM Ingredients WHERE ingredient_ID = :id:";
        $ing = $app->modelsManager->executeQuery
        (
            $phql,
            array('id' => $id)
        )->getFirst();

        return getresponse(ingdata($ing));
    });



/************** Step ********************/

/**
 * Get a Recipe by its code and return all instructions and ingredients
 */
    $app->get('/api/step/{code}', function ($code) use ($app)
    {

        global $ingphql, $reinstphql;

        //Get recipes and instructions for recipe with the specified code
        $recipeinstructions = $app->modelsManager->executeQuery
        (
            $reinstphql,
            array('code' => $code)
        );
        //Get ingredients from the recipe with the specified code
        $ingredients = $app->modelsManager->executeQuery
        (
            $ingphql,
            array('code' => $code)
        );
        //Get JSON response
        return getfullresponse($recipeinstructions,$ingredients);
    });

/**
 * Return a default recipe that does not require Naturebake supplies. Useful to display
 * for when users enter the wrong code
 */
    $app->get('/api/step/default-bake', function () use ($app)
    {

        global $ingphql, $reinstphql;

        //Get ingredients for default bake recipe
        $ingredients = $app->modelsManager->executeQuery
        (
            $ingphql,
            array('code' => 'default-bake')
        );

        //Get Recipes and Instructions for default bake recipe
        $recipeinstructions = $app->modelsManager->executeQuery
        (
            $reinstphql,
            array('code' => 'default-bake')
        );

        //Return JSON response containing recipe, instructions and ingredients
        return getfullresponse($recipeinstructions,$ingredients);
    });

    $app->handle();

/**
 * Return a response for a recipe with its associated instructions and ingredients
 * @param recipeinstructions: RecipeInstructions database object
 * @param ing: Ingrediants Object
 * @return array: JSON response
 */
    function getfullresponse($recipeinstructions,$ing)
    {
        global $found, $notfound;
        $response = new Response();
        if (iterator_count($recipeinstructions) == 0)
        {
            $response->setJsonContent
            (
                array('status' => $notfound)
            );
        }
        else
        {
            $instructions = array();
            $ingredients = array();
            // Collect all instructions for the recipe
            foreach ($recipeinstructions as $row)
            {
                $instructions[] = instdata($row->I);
            }
            //Collect all the ingredients for the recipe
            foreach ($ing as $row)
            {
                $ingredients[] = ingdata($row);
            }
            //Set response with recipe instructions and ingredients
            $response->setJsonContent
            (
                array
                (
                    'status' => $found,
                    'recipe'   => recipedata($recipeinstructions[0]->R),
                    'instructions' => $instructions,
                    'ingredients' => $ingredients
                )
            );
        }
        return $response;
    }


/**
 * Return a response for a Database object
 * @param: dbdata: Array of data for response
 * @return array: JSON reponse
 */
    function getresponse($dbdata)
    {
        global $found, $notfound;
        $response = new Response();
        if (!$dbdata)
        {
            //Set response as now found
            $response->setJsonContent
            (
                array('status' => $notfound)
            );
        }
        else
        {
            //Set response with data
            $response->setJsonContent
            (
                array
                (
                    'status' => $found,
                    'data'   => $dbdata
                )
            );
        }
        return $response;
    }

/**
 * Get data from a ingredients database object
 * @param ing: Ingredient object
 * @return array: Ingredient data
 */
    function ingdata($ing)
    {
        if(!$ing)
        {
            return FALSE;
        }
        return array
        (
            'id'   => $ing->ingredient_ID,
            'name' => $ing->name,
            'container' => $ing->container,
            'allergens' => $ing->allergens,
        );
    }

/**
 * Get data from a Instructions database object
 * @param inst: Instruction object
 * @return array: Instruction data
 */
    function instdata($inst)
    {
        if(!$inst)
        {
            return FALSE;
        }
        return array
        (
            'id'   => $inst->instruction_ID,
            'title' => $inst->title,
            'description' => $inst->description,
        );
    }

/**
 * Get data from a Recipe database object
 * @param $recipe: Recipe object
 * @return array|bool: Recipe data, or FALSE if no data to return.
 */
    function recipedata($recipe)
    {
        global $imagepath;

        if(!$recipe)
        {
            return FALSE;
        }
        //Encode image so that it can be sent in JSON response
        $imagedata = file_get_contents($imagepath . $recipe->image);
        $base64 = base64_encode($imagedata);
        return array
        (
            'id'   => $recipe->recipe_ID,
            'code' => $recipe->code,
            'name' => $recipe->name,
            'serving' => $recipe->serving,
            'calories' => $recipe->calories,
            'cookingtime' => $recipe->cookingtime,
            'youtube' => $recipe->youtube,
            'date' => $recipe->date,
            'image' => $base64

        );
    }

?>