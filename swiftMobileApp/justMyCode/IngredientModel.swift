//
//  IngredientModel.swift
//  NatureBake
//
//  Created by Chris Taylor on 05/03/2016.
//  Copyright © 2016 naturebake. All rights reserved.
//  Chris Taylor <c.p.d.taylor@ncl.ac.uk>
//  Student Number: 120121386

import Foundation

/**
 Class containing properties of a ingredient model.
 
 - parameter id: database record id
 - parameter name: name of ingredient
 - parameter name: Container in naturebakebox that contains the ingredient
 - parameter allergen: Boolean value describing if ingredient is an allergen or not

 */
class IngredientModel: NSObject
{
    var id : String?
    var name: String?
    var container: String?
    var allergen: Bool
    
    init(id:String, name: String, container: String, allergen: Bool)
    {
        self.id = id
        self.name = name
        self.container = container
        self.allergen = allergen
    }
    
    // Return description of an ingredient
    override var description: String
    {
        return "Name: \(name), Container: \(container), Allergen: \(allergen)"
    }
    
    
}