//
//  MapViewController.swift
//  NatureBake
//
//  Created by Chris Taylor on 25/02/2016.
//  Copyright © 2016 naturebake. All rights reserved.
// 
//  Annotation code adapted from http://www.raywenderlich.com/87008/overlay-views-mapkit-swift-tutorial
//  Chris Taylor <c.p.d.taylor@ncl.ac.uk>
//  Student Number: 120121386

import UIKit
import MapKit


/**
A View controller for the Maps screen
**/
class MapViewController: UIViewController, MKMapViewDelegate
{

    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    
    //Load coordiants for Newcastle into the Newcastle model
    var newcastle = Coordinates(filename: "Newcastle")

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.mapView.delegate = self
        
        // Get coordiants for Newcastle
        let latDelta = self.newcastle.overlayTopLeftCoordinate.latitude -
            self.newcastle.overlayBottomRightCoordinate.latitude
        let span = MKCoordinateSpanMake(fabs(latDelta), 0.0)
        let region = MKCoordinateRegionMake(self.newcastle.midCoordinate, span)
        
        self.mapView.region = region
        
        //Add custom made resturant annotations (vegan and dairy free)
        addResturantPins()
    }

    
    
/**
     Add Vegan and Dairy free pins found in NewcastleResturnts.plist
*/
    func addResturantPins()
    {
        //Get data from NewcastleResturants.plist
        let filePath = NSBundle.mainBundle().pathForResource("NewcastleResturants", ofType: "plist")
        let res = NSArray(contentsOfFile: filePath!)
        //For every resturant/cafe
        for r in res!
        {
            let point = CGPointFromString(r["location"] as! String)
            // Get coordiantes
            let coordinate = CLLocationCoordinate2DMake(CLLocationDegrees(point.x), CLLocationDegrees(point.y))
            let title = r["name"] as! String
            //Get the type number - what type of resturant/cafe it is
            let typeRawValue = Int((r["type"] as! String))
            let type = ResturantType(rawValue: typeRawValue!)!
            let subtitle = r["subtitle"] as! String
            let annotation = ResturantAnnotation(coordinate: coordinate, title: title, subtitle: subtitle, type: type)
            self.mapView.addAnnotation(annotation)
        }
    }

/**
     Specify to use the ResturantAnnotationView for the MKAnnotationView
     - return : return custom annotaton view for the map
*/
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
    {
        let annotationView = ResturantAnnotationView(annotation: annotation, reuseIdentifier: "Attraction",dummy: "")
        annotationView.canShowCallout = true
        return annotationView
    }
    
/**
     Change the map type based on the type a user chooses from a segmented control object.
*/
    @IBAction func segmentControlChanged(sender: UISegmentedControl)
    {
        switch sender.selectedSegmentIndex
        {
        case 1:
            self.mapView.mapType = MKMapType.SatelliteFlyover
        case 2:
            self.mapView.mapType = MKMapType.HybridFlyover
        default:
            self.mapView.mapType = MKMapType.Standard
        }
        
    }
    
}
