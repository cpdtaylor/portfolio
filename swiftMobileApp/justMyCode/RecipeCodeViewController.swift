//
//  RecipeCodeViewController.swift
//  NatureBake
//
//  Created by Chris Taylor on 25/02/2016.
//  Copyright © 2016 naturebake. All rights reserved.
//  Chris Taylor <c.p.d.taylor@ncl.ac.uk>
//  Student Number: 120121386

import UIKit


/**
 View controller for the RecipeCode entry screen
*/
class RecipeCodeViewController: UIViewController
{
    
    @IBOutlet weak var recipeInput: UITextField!
    
/**
Remove keyboard once button has been pressed.
 - parameter sender: The "Enter" button
*/
    @IBAction func buttonPressed(sender: UIButton)
    {
        self.recipeInput.resignFirstResponder()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

/**
 Remove keyboard when screen is pressed.
*/
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        self.view.endEditing(true)
    }
    
/**
If changing to RecipeViewController then set the code as the input text so recipe can be queried.
*/
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "RecipeCodeEntry"
        {
            if let recipeViewController = segue.destinationViewController as? RecipeViewController
            {
                recipeViewController.code = self.recipeInput.text!
            }
        }
    }

}
