//
//  InstructionModel.swift
//  NatureBake
//
//  Created by Chris Taylor on 05/03/2016.
//  Copyright © 2016 naturebake. All rights reserved.
//  Chris Taylor <c.p.d.taylor@ncl.ac.uk>
//  Student Number: 120121386

import Foundation

/**
Class containing properties of a Instruction model.

- parameter id: database record id
- parameter title: Short Word for Instruction
- parameter description: Entire instruction

*/
class InstructionModel: NSObject
{
    var id : String?
    var title: String?
    var des: String?
    
    init(id:String, title: String, description: String)
    {
        self.id = id
        self.title = title
        self.des = description
    }
    
    // Return description of an Instruction
    override var description: String
    {
        return "Title: \(title), Description: \(des)"
    }
    
    
}