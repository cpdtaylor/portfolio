//
//  Rest.swift
//  NatureBake
//
//  Created by Chris Taylor on 27/02/2016.
//  Copyright © 2016 naturebake. All rights reserved.
//  Chris Taylor <c.p.d.taylor@ncl.ac.uk>
//  Student Number: 120121386

import Foundation

/**
Defines REST URIs for the Naturebake server

Currently these are only the localhost for testing purposes
*/
class Rest
{
    //Get all recipes
    let viewRecipes = "http://localhost/naturebakeapp/api/recipes"
    //Get specific recipe and its instructions and ingredients
    let getRecipe = "http://localhost/naturebakeapp/api/step/"
    // Get default recipe - for when recipe code entry is incorrect
    let getDefaultRecipe = "http://localhost/naturebakeapp/api/step/default-bake"

}