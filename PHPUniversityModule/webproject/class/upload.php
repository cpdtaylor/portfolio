<?php

/**
 * Class that provides functionality for editing and uploading different types of publications
 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
 * Student Number: 120121386
 *
 */
    class Upload extends Siteaction
    {
/**
 * Upload limit for files
 * @var int
 */
        private $uploadlimit = 10000000;  //10Mb

/**
 * Handle upload related tasks
 * @param object    $context    The context object for the site
 *
 */
        public function handle($context)
        {
            if ($context->hasuploader())
            {
                $template = 'uploads/upload.twig';
                $type = $context->local()->document();
                $extensions = array('application/pdf' => 'pdf');
                $canfupload = TRUE;
                $title = 'Upload a Document';
                $rest = $context->rest();

                switch ($rest[0])
                {
                    case 'edit':
                        return $this->editpub($context);

                    case $context->local()->video():
                        $type = $context->local()->video();
                        $extensions = array('video/mp4' => 'mp4');
                        $title = 'Upload a Video';
                        break;

                    case $context->local()->app():
                        $type = $context->local()->app();
                        $extensions = array(); // Just links
                        $canfupload = FALSE; //Dont allow file uploads
                        $title = 'Upload an App';
                        break;

                    case $context->local()->image():
                        $type = $context->local()->image();
                        $extensions = array('image/png' => 'png', 'image/jpeg' => 'jpg');
                        $title = 'Upload a Image';
                        break;

                    case $context->local()->source():
                        $type = $context->local()->source();
                        $extensions = array('application/x-compressed' => 'zip', 'application/x-zip-compressed' => 'zip',
                        'application/zip' => 'zip', 'multipart/x-zip' => 'zip');
                        $title = 'Upload Source';
                        break;

                    case $context->local()->data():
                        $type = $context->local()->data();
                        $extensions = array('application/x-compressed' => 'zip', 'application/x-zip-compressed' => 'zip',
                            'application/zip' => 'zip', 'multipart/x-zip' => 'zip');
                        $title = 'Upload Data';
                        break;

                    default :
                        break;
                }

                $context->local()->addval('canfupload',$canfupload); //Can you do a file upload?
                $context->local()->addval('title',$title);
                $context->local()->addval('uploadtype',$type); //Form action should point to this page

                if ($_SERVER['REQUEST_METHOD'] === 'POST')
                {
                    return $this->uploadpub($context,$type,$extensions,$template);
                }

                return $template;
            }
            else
            {
                (new Web)->noaccess(); //Should not be accessing upload page
            }
        }

/**
 * Uploads a publication to the local filesystem and attaches a publication record in the database.
 * @param $context
 * @param $type
 * @param $extensions
 * @param $template
 * @return mixed
 * @throws \RedBeanPHP\RedException
 */
        private function uploadpub($context,$type,$extensions,$template)
        {
            $local = $context->local();

            //No need for SQL Injection protection as Red bean provides functionality for that.
            $publication = R::dispense('publication');

            $publication->type = $type; //type of the publication

            $title = $context->mustpostpar('title');
            $description = $context->mustpostpar('description');
            $licencing = $context->mustpostpar('licencing');
            $firstname = $context->mustpostpar('firstname');
            $lastname = $context->mustpostpar('lastname');
            $email = $context->mustpostpar('email');

            //Check errors for generic inputs
            $inputerrors = $this->checkInputs($title,$description,$licencing,$firstname,$lastname,$email);
            if (!empty($inputerrors))
            {
                foreach ($inputerrors as $ie)
                {
                    $context->local()->message('errmessage', $ie);
                }
                return $template;

            }

            $publication->title = $title;
            $publication->description = $description;
            $publication->licencing = $licencing;

            $author = R::dispense('author');
            $author->firstname = $firstname;
            $author->lastname = $lastname;
            $author->email = $email;
            $author->sharedPublication[] = $publication; //Many-to-Many relationship

            //Process tags
            $tags = $context->postpar('tags','');
            $tagbeans = $this->gettagbeans($tags,$publication);
            $publication->time = $context->utcnow();


            $link = $context->postpar('link','');

            if (file_exists($_FILES['uploadfile']['tmp_name']) || is_uploaded_file($_FILES['uploadfile']['tmp_name']))
            {
                //Do a file upload
                if ($_FILES['uploadfile']['error'] > 0)
                {
                    $local->message('errmessage','An unexpected error occured');
                    return $template;

                }
                $uploadsize = $_FILES['uploadfile']['size'];
                $file = basename($_FILES['uploadfile']['name']);
                $tmp_file = $_FILES['uploadfile']['tmp_name'];

                if ($uploadsize == 0)
                {
                    $local->message('errmessage', 'You have uploaded an empty file.');
                    return $template;
                }
                if ($uploadsize > $this->uploadlimit)
                {
                    $local->message('errmessage', 'Your ' . $type . ' exceeds the upload limit of: ' . ($this->uploadlimit / 1000) . "Kb");
                    return $template;
                }

                //Is the file a good format?
                if ($this->goodfile($file,$tmp_file,$extensions))
                {
                    $datedir = $this->getdatedir($this->typedir($type,$context->local()));
                    $newfile = $this->makeunique($file,$context->user()->getID());
                    $targetdir = $this->createupdir($local->basedir() . $datedir);
                    $targetpath = $targetdir . $newfile;

                    //Does a file somehow already exist?
                    if (file_exists($targetpath))
                    {
                        $local->message('errmessage', 'The ' . $type . ' you are uploading already exists');
                        return $template;
                    }
                    //Did the file upload?
                    if (move_uploaded_file($tmp_file, $targetpath))
                    {
                        //SUCCESS
                        $filebean = R::dispense('file');
                        $filebean->filepath = $datedir . $newfile;
                        $filebean->originalname = $file;
                        $filebean->filesize = $uploadsize;
                        $publication->file = $filebean;
                        $uploader = $context->user();
                        $uploader->ownPublication[] = $publication;
                        R::store($author);
                        R::store($uploader);
                        if ($tagbeans !== FALSE)
                        {
                            R::storeAll($tagbeans);
                        }
                        R::store($publication);
                        $local->message('smessage', 'Your ' . $type . ' has been successfully uploaded');

                    }
                    else
                    {
                        $local->message('errmessage', 'Your ' . $type . ' failed to upload');

                    }
                    return $template;

                }
                else
                {
                    //Wrong file format
                    $local->message('errmessage','You uploaded a ' . $type . ' with the wrong extension or file format.
                    Please either upload a .' . implode(' or .', $extensions)); //impode on values
                    return $template;
                }
            }
            else
            {
                //Is this a correct link?
                if ($this->checklink($link))
                {
                    $publication->url = $link;
                    R::store($author);
                    $uploader = $context->user();
                    $uploader->ownPublication[] = $publication;
                    R::store($uploader);
                    if ($tagbeans !== FALSE)
                    {
                        R::storeAll($tagbeans);
                    }
                    R::store($publication);
                    $local->message('smessage', 'Thank you for your upload.');
                }
                else
                {
                    $local->message('errmessage', 'The link you provided "' . $link . '" is not valid');
                }
                return $template;

            }

        }

/**
 * Edit a publication
 * @param $context
 * @return string
 */
        private function editpub($context)
        {
            $rest = $context->rest();
            $template = 'uploads/editpub.twig';
            if (count($rest) >= 2)
            {
                $pubid = $rest[1];
                //Should only allow numeric input
                if (is_numeric($pubid))
                {
                    $pub = R::load('publication', $pubid);
                    $context->local()->addval('pub',$pub);
                    $canfupload = TRUE;
                    $pubtype = $pub->type();
                    if ($pubtype == $context->local()->app())
                    {
                        $canfupload = FALSE;
                    }
                    $context->local()->addval('canfupload',$canfupload);
                    $context->local()->addval('title', 'Edit Publication');
                    $context->local()->addval('uploadtype',$pubtype);

                    if ($_SERVER['REQUEST_METHOD'] === 'POST')
                    {
                        $title = $context->mustpostpar('title');
                        $description = $context->mustpostpar('description');
                        $licencing = $context->mustpostpar('licencing');
                        $firstname = $context->mustpostpar('firstname');
                        $lastname = $context->mustpostpar('lastname');
                        $email = $context->mustpostpar('email');

                        $tags = $context->postpar('tags','');
                        $tagbeans = $this->gettagbeans($tags,$pub);
                        $pub->time = $context->utcnow();
                        $inputerrors = $this->checkInputs($title,$description,$licencing,$firstname,$lastname,$email);
                        if (!empty($inputerrors))
                        {
                            foreach ($inputerrors as $ie)
                            {
                                $context->local()->message('errmessage', $ie);
                            }
                            return $template;

                        }
                        else
                        {
                            $pub->title = $title;
                            $pub->description = $description;
                            $pub->licencing = $licencing;
                            $authors = $pub->authors(); //Only one author currently
                            foreach ($authors as $a)
                            {
                                $a->firstname = $firstname;
                                $a->lastname = $lastname;
                                $a->email = $email;
                                $a->sharedPublication[] = $pub; //Many-to-Many relationship
                                R::store($a);
                            }
                            if ($tagbeans !== FALSE)
                            {
                                R::storeAll($tagbeans);
                            }
                            R::store($pub);
                            $context->local()->message('smessage', 'You have successfully edited the publication');
                        }

                    }
                    return $template;
                }
                else
                {
                    (new Web)->bad();
                }

            }
            else
            {
                (new Web)->bad();
            }
        }

/**
 * Create an upload directory if one for the current date does not exist
 *
 * @param $basedir
 * @return string  Return the upload directory
 */
        private function createupdir($dir)
        {
            if (!file_exists($dir))
            {
                mkdir($dir, 0755, true);
            }

            return $dir;
        }


/**
 * get a date dir
 * @param dir
 * @return string  Return the date directory
 */
        private function getdatedir($dir)
        {
            $datedir = $dir.date('/Y/m/d/');
            return $datedir;
        }


/**
 * Check if a file has the correct extension and mimetype
 * @param $file $uploaded_file
 * @return bool
 */
        private function goodfile($file,$uploaded_file,$extentions)
        {
            $file_parts = pathinfo($file);
            $extention = $file_parts['extension'];
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $uploaded_file);
            $goodfile = FALSE;
            foreach ($extentions as $k => $v)
            {
                if ($mime === $k )
                {
                    if ($extention === $v)
                    {
                        $goodfile = TRUE;
                    }
                }
            }
            finfo_close($finfo);
            return($goodfile);
        }

/**
 * Make a filename unique to avoid file collisons and add uploaderid
 * @param $file
 * @return string
 */
        private function makeunique($file,$uploaderid)
        {
            $file_parts = pathinfo($file);
            return  $uploaderid . '_' . uniqid() . '.' . $file_parts['extension'];
        }

/**
 * Get only hashtags and return false if none found.
 * @param $string
 * @return array|bool
 */
        private function gethashtags($string)
        {
            //http://stackoverflow.com/questions/3060601/retrieve-all-hashtags-from-a-tweet-in-a-php-function
            $hashtags = FALSE;
            preg_match_all("/(#\w+)/u", $string, $matches);
            if ($matches)
            {
                $hashtagsarray = array_count_values($matches[0]);
                $hashtags = array_keys($hashtagsarray);
            }
            return $hashtags;
        }

/**
 * Given a string and a publication, assign any hashtags defined in the string to the publication
 * Sets up tags to have a many-many relationship with publications.
 * @param $string
 * @param $publication
 * @return array|bool
 * Return an array of tag beans if there are hashtags to parse
 * Return FALSE if there are no hashtags in the string.
 * @throws \RedBeanPHP\RedException
 */
        private function gettagbeans($string,$publication)
        {
            $beans = FALSE;
            $hashtags = $this->gethashtags($string);
            if ($hashtags !== FALSE)
            {
                $beans = array();
                foreach ($hashtags as $h)
                {
                    $tagbean = R::dispense('tag');
                    //Remove hash so tag content is kept. make lowercase
                    $tagbean->name = strtolower(trim($h,'#'));
                    $tagbean->sharedPublication[] = $publication;
                    array_push($beans, $tagbean);
                }
            }
            return $beans;

        }

/**
 * Check that a url is formatted correctly and is not an empty string
 * @param $link
 * @return bool
 */
        private function checklink($link)
        {
            return filter_var($link, FILTER_VALIDATE_URL) && $link != '';
        }


/**
 * Check format for generic text inputs and author attributes
 * @param $title
 * @param $description
 * @param $licening
 * @return error messages. empty array if none
 */
        private function checkInputs($title,$description,$licencing,$firstname,$lastname,$email)
        {
            $errmess = array();
            $ans = '/[^a-z0-9 .]/i';
            $alpha = "/[^a-z]/i";

            if (preg_match($ans, $title))
            {
                $errmess[] = 'The publication title can only contain letters,numbers spaces and dots.';
            }
            if (preg_match($ans, $description))
            {
                $errmess[] = 'The publication description can only contain letters,numbers spaces and dots.';
            }
            if (preg_match($ans, $licencing))
            {
                $errmess[] = 'The publication licencing can only contain letters,numbers spaces and dots.';
            }
            if (preg_match($alpha, $firstname))
            {
                $errmess[] = 'The author firstname can only contain letters';
            }
            if (preg_match($alpha, $lastname))
            {
                $errmess[] = 'The author lastname can only contain letters';
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                $errmess[] = 'Please provide a valid email address for the author';
            }
            return $errmess;
        }


/**
 * Return the path to directory for a given publication type
 *
 * @return string
 */
        private function typedir($type,$local)
        {
            $path = null;
            switch ($type)
            {
                case $local->document():
                    $path = '/assets/uploads/docs/';
                    break;

                case $local->video():
                    $path = '/assets/uploads/videos/';
                    break;

                case $local->image():
                    $path = '/assets/uploads/images/';
                    break;

                case $local->app():
                    $path = null; //no uploads
                    break;

                case $local->data():
                    $path = '/assets/uploads/data/';
                    break;

                case $local->source():
                    $path = '/assets/uploads/sourcecode/';
                    break;

                default :
                    break;
            }
            return $path;
        }

    }
?>