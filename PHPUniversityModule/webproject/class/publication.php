<?php
/**
 * Contains definition of Publication class
 *
 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
 * @copyright 2012-2015 Newcastle University
 */
/**
 * A class that contains code to handle viewing and downloading of individual publications.
 * Any user can view or download a publication so no access control required in this class.
 */
    class Publication extends Siteaction
    {
/**
 * Handle publication id for viewing/downloading
 *
 * @param object $context The context object for the site
 *
 * @return string    A template name
 *
 */
        public function handle($context)
        {

            $rest = $context->rest();
            //download request
            if ($rest[0] == 'download')
            {
                //Should be an id specified
                if (count($rest) >= 2)
                {
                    $pubid = $rest[1];
                    //Should only allow numeric input
                    if (is_numeric($pubid))
                    {
                        $pub = R::load('publication', $pubid);
                        $filepath = $context->local()->basedir() . $pub->file()->filepath();
                        $context->sendfile($filepath,$pub->file()->filename());
                    }
                    else
                    {
                        (new Web)->bad();
                    }

                }
                else
                {
                    (new Web)->bad();
                }

            }
            else
            {
                $pubid = $rest[0];
                //check that number entered only contains digits (numerical id)
                if (is_numeric($pubid))
                {
                    $pub = R::load('publication', $pubid);
                    $context->local()->addval('pub',$pub);
                    return 'publications/publication.twig';
                }
                else
                {
                    (new Web)->bad();
                }
            }

        }

    }
?>
