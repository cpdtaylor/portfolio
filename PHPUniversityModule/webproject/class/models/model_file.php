<?php
/**
 * A model class for the RedBean object File
 *
 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
 * @copyright 2015 Newcastle University
 *
 */
/**
 * A class implementing a RedBean model for Publication beans
 */
    class Model_File extends RedBean_SimpleModel
    {
/**
 * Return filename
 *
 * @return object
 */
        public function filename()
        {
            return $this->bean->originalname;
        }
/**
 * Return filepath
 *
 * @return object
 */
        public function filepath()
        {
            return $this->bean->filepath;
        }
/**
 * Return filesize
 *
 * @return object
 */
        public function filesize()
        {
            return round($this->bean->filesize / 1000000,2) . 'Mb';
        }


    }
?>

