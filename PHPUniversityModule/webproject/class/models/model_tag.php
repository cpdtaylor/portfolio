<?php
/**
 * A model class for the RedBean object Tag
 *
 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
 * @copyright 2015 Newcastle University
 *
 */
/**
 * A class implementing a RedBean model for Publication beans
 */
    class Model_Tag extends RedBean_SimpleModel
    {

/**
 * Return name
 *
 * @return object
 */
        public function name()
        {
            return $this->bean->name;
        }

    }
?>

