<?php
/**
 * A model class for the RedBean object Author
 *
 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
 * @copyright 2015 Newcastle University
 *
 */
/**
 * A class implementing a RedBean model for Publication beans
 */
    class Model_Author extends RedBean_SimpleModel
    {
/**
 * Return fullname
 *
 * @return object
 */
        public function fullname()
        {
            return $this->bean->firstname . ' ' . $this->bean->lastname;
        }

/**
 * Return firstname
 *
 * @return object
 */
        public function firstname()
        {
            return $this->bean->firstname;
        }

/**
 * Return lastname
 *
 * @return object
 */
        public function lastname()
        {
            return $this->bean->lastname;
        }

/**
 * Return email
 *
 * @return object
 */
        public function email()
        {
            return $this->bean->email;
        }

    }
?>

