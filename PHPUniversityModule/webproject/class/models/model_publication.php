<?php
/**
 * A model class for the RedBean object Publication
 *
 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
 * @copyright 2015 Newcastle University
 *
 */
/**
 * A class implementing a RedBean model for Publication beans
 */
    class Model_Publication extends RedBean_SimpleModel
    {
/**
 * Return title
 *
 * @return object
 */
        public function title()
        {
            return $this->bean->title;
        }
/**
 * Return type
 *
 * @return object
 */
        public function type()
        {
            return $this->bean->type;
        }
/**
 * Return time
 *
 * @return object
 */
        public function time()
        {
            return $this->bean->time;
        }
/**
 * Return url.
 *
 * @return object
 */
        public function url()
        {
            return $this->bean->url;
        }

/**
 * Return an array of authors
 * @return array
 */
        public function authors()
        {
            return $this->bean->sharedAuthorList;
        }

/**
 * Return description
 * @return object
 */
        public function description()
        {
            return $this->bean->description;
        }

/**
 * Return licencing
 * @return mixed
 */
        public function licencing()
        {
            return $this->bean->licencing;
        }

/**
 * Return the uploader of the publication
 * @return mixed
 */
        public function uploader()
        {
            return $this->bean->user->id;
        }

/**
 * Return the file
 * @return mixed
 */
        public function file()
        {
            return $this->bean->file;
        }

/**
 * Return an array of tags
 * @return mixed
 */
        public function tags()
        {
            return $this->bean->sharedTagList;
        }

    }
?>

