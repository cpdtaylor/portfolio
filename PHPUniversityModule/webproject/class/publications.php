<?php
/**
 * Contains definition of Publications class
 *
 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
 * @copyright 2012-2015 Newcastle University
 */
/**
 * A class that contains code to handle any publication list related request, for example displaying a list of document
 * publications
 */
    class Publications extends Siteaction
    {
/**
 * Page size constant
 * @var int
 */
        const PAGESIZE = 3;
/**
 * Handle listing of publication requests including myuploads and search uploads
 *
 * @param object	$context	The context object for the site
 *
 * @return string	A template name
 *
 */
        public function handle($context)
        {
            $title = '';
            $type = '';
            $template = 'publications/publications.twig';
            $rest = $context->rest();
            switch ($rest[0])
            {
                case 'myuploads':
                    return $this->myuploads($context);

                case 'search':
                    return $this->search($context);

                case $context->local()->document():
                    $title = 'Documents';
                    $type = $context->local()->document();
                    break;


                case $context->local()->video():
                    $title = 'Videos';
                    $type = $context->local()->video();
                    break;

                case $context->local()->image():
                    $title = 'Images';
                    $type = $context->local()->image();
                    break;

                case $context->local()->data():
                    $title = 'Data';
                    $type = $context->local()->data();
                    break;

                case $context->local()->source():
                    $title = 'Source Code';
                    $type = $context->local()->source();
                    break;

                case $context->local()->app():
                    $title = 'Apps';
                    $type = $context->local()->app();
                    break;

                default :
                    $template = 'index';
                    break;
            }

            $context->local()->addval('title',$title);
            $context->local()->addval('type',$type);
            $pagesize = $context->getpar('pagesize', self::PAGESIZE);
            //Count publications of a type
            $pages = intval((R::count('publication', ' type = ? ',array($type)) + $pagesize) / $pagesize);
            $context->local()->addval('pages', $pages);
            $context->local()->addval('pagesize', $pagesize);
            $context->local()->addval('page', $context->getpar('page', 1));
            return $template;
        }

/**
 *
 * Handle rendering of template for myuploads.
 *
 * @param $context
 * @return string|void
 */
        public function myuploads($context)
        {
            //Check that user can upload. If cant they wont have an uploads page
            if ($context->hasuploader())
            {
                $user = $context->user()->fresh(); //refresh bean
                $pubs = $user->ownPublication;
                $context->local()->addval('pubs',$pubs);
                return 'publications/myuploads.twig';
            }
            else
            {
                return (new Web)->noaccess(); //Should not be accessing someone else's uploads
            }
        }

/**
 * Handle search functionality
 * Takes a tag and finds all publications that have that tag and renders them in search.twig
 * @param $context
 * @return string
 */
        public function search($context)
        {
            //search form should use GET. Can copy searches from url.
            $tagstr = $context->mustgetpar('search');
            $tagstr = preg_split('/\s+/', trim($tagstr)); //get all tag searches
            //silently take first search value, make lower case so case insensetive search. tags are LC
            $tag = strtolower($tagstr[0]);
            $tagbean = R::findOne('tag', 'name = ?',array($tag));
            $context->local()->addval('title','Search For: ' . $tag);
            //If found a tag
            if ($tagbean !== NULL)
            {
                $pubs = $tagbean->sharedPublication;
                $context->local()->addval('pubs',$pubs);
            }
            return 'publications/search.twig';


        }

    }
?>
