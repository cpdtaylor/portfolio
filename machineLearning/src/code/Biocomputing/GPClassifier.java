package Biocomputing;

import Biocomputing.ModuloD;
import Biocomputing.Sigmoid;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

import org.jgap.InvalidConfigurationException;
import org.jgap.event.GeneticEvent;
import org.jgap.event.GeneticEventListener;
import org.jgap.gp.CommandGene;
import org.jgap.gp.GPProblem;
import org.jgap.gp.IGPProgram;
import org.jgap.gp.function.Abs;
import org.jgap.gp.function.Add;
import org.jgap.gp.function.And;
import org.jgap.gp.function.ArcCosine;
import org.jgap.gp.function.ArcSine;
import org.jgap.gp.function.ArcTangent;
import org.jgap.gp.function.Ceil;
import org.jgap.gp.function.Cosine;
import org.jgap.gp.function.Divide;
import org.jgap.gp.function.Equals;
import org.jgap.gp.function.Exp;
import org.jgap.gp.function.Floor;
import org.jgap.gp.function.GreaterThan;
import org.jgap.gp.function.If;
import org.jgap.gp.function.IfElse;
import org.jgap.gp.function.Log;
import org.jgap.gp.function.Modulo;
import org.jgap.gp.function.Multiply;
import org.jgap.gp.function.Multiply3;
import org.jgap.gp.function.Not;
import org.jgap.gp.function.Or;
import org.jgap.gp.function.Pow;
import org.jgap.gp.function.Round;
import org.jgap.gp.function.Sine;
import org.jgap.gp.function.Subtract;
import org.jgap.gp.function.Tangent;
import org.jgap.gp.function.Xor;
import org.jgap.gp.impl.DeltaGPFitnessEvaluator;
import org.jgap.gp.impl.GPConfiguration;
import org.jgap.gp.impl.GPGenotype;
import org.jgap.gp.impl.TournamentSelector;
import org.jgap.gp.terminal.Terminal;
import org.jgap.gp.terminal.Variable;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;

/**
 * A genetic program classifier. 
 * @author Chris Taylor
 * Student Number: 120121386
 */
public class GPClassifier extends Classifier {
    
	
	private int numAtt;
	private int numClasses;
	private boolean binaryClassifier;
	private PrintWriter itfitwriter; //Used for outputing results iteration/fitness for report
	private PrintWriter timefitwriter; //Used for outputing results time/fitness for report
	private PrintWriter solutionwriter; //Used for outputing solution stats for report
	private final boolean WRITERESULTS = false; // Write results to file?

	
	private int evolutionNumber = 200;
	
	private GeneticProgram geneticProgram;
	private IGPProgram solution; 
	private GPGenotype gp;
	
	/**
	 * Constructor for GPClassifier
	 * @param trainingSet
	 * @throws InvalidConfigurationException 
	 */
	public GPClassifier(InstanceSet trainingSet)
	{
		int numAtt=Attributes.getNumAttributes();	
		this.numAtt = numAtt;
		int numClasses=Attributes.numClasses;
		this.numClasses = numClasses;
		this.binaryClassifier = this.isBinary(numClasses);
		try
		{
			GeneticProgram problem = new GeneticProgram(numAtt,this.binaryClassifier,trainingSet);
	        GPGenotype gp = problem.create();
	        gp.setVerboseOutput(true);
	        
	        gp.evolve(this.evolutionNumber);
	
	        IGPProgram p = gp.getAllTimeBest();
	        this.gp = gp;
	        this.geneticProgram = problem;
	        this.solution = p;
	        
	        //testGeneticProgram(trainingSet);
	        
	        if(WRITERESULTS)
	        {
		        this.itfitwriter.close();
		        this.timefitwriter.close();
		        this.solutionwriter.close();
	        }

		}
		catch(InvalidConfigurationException e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * Function to test genetic program predictive power
	 * @param testData
	 */
	public void testGeneticProgram(InstanceSet testData)
	{
		Instance[] instances = testData.getInstances();
    
		for(int i = 0; i < instances.length; i++ )
		{
			double[] attrs = instances[i].getRealAttributes();
			int c = instances[i].getClassValue();
			if(attrs.length > this.numAtt)
			{//truncate array to correct number of input parameters. Perhaps bug in getRealAttributes?
				attrs = Arrays.copyOf(attrs, attrs.length-(attrs.length - this.numAtt));
			}
			double testResult = this.geneticProgram.computeInstance(this.solution, attrs);
			System.out.print("Inputs: " + Arrays.toString(attrs));
	        System.out.print("  Output: " + c);
	        System.out.println ("  Results: " + testResult);
	        	
		}
		
	}
	
	/**
	 * Is classifier binary?
	 * @param classCount
	 * @return
	 * @throws Exception
	 */
	private boolean isBinary(int classCount) throws IllegalArgumentException
	{	
		if(classCount <=1)
		{
			throw new IllegalArgumentException("Can not have 1 or less classes in a trainingSet?");
		}
		else if(classCount == 2)
		{
			return true;
		}
		else
		{
			return false; 
		}
	}
	
	
	@Override
	public int classifyInstance(Instance ins) {
		
		double[] attributes = ins.getRealAttributes();
		if(attributes.length > this.numAtt)
		{//truncate array to correct number of input parameters. Perhaps bug in getRealAttributes?
			attributes = Arrays.copyOf(attributes, attributes.length-(attributes.length - this.numAtt));
		}
		
        double testResult = Math.abs(this.geneticProgram.computeInstance(this.solution, attributes));
        int c = -1;
		int retval = Double.compare(testResult, 0.5);
		if(retval > 0) 
		{
			c = 1;
		}
		else if(retval <= 0) // A MLP will *always classify an instance. 
		{
			c = 0;
		}			
		
		return c;
	}

	@Override
	public void printClassifier() {
		
		System.out.println("The number of attributes is: " + this.numAtt);
		System.out.println("The number of classes is: " + this.numClasses);
        this.gp.outputSolution(this.solution);

	}
	
	/**
	 * Wrapping a genetic program which uses JGAP
	 * A Genetic Program for continues attributes
	 * @author Chris Taylor
	 * Student Number: 120121386
	 */
	private class GeneticProgram extends GPProblem{
		
		private int numAttr;
		private boolean isBinary;
		
		private double[][] attributes;
		private double[][] output;
		private Variable[] variables;
		
		private int maximumInitiationDepth = 4;
		private int populationSize = 200;
		private int maxDepth = 8;
		private double crossoverProbability = 0.9;
		private double mutationProbability = 0.15;
		private int maximumNumNodes = 150;
		private int tournamentSelectorSize = 0;
		private double bloatconstant = 0.05;

		
		private String[] booleanCommands = new String[]
				{/*"If","AndD","OrD","GreaterThanD",/*"EqualsD"*/};
		
		private String[] finalCommands = new String[]
				{"Sine","Add","Abs","Round","ModuloD",/*"Multiply",*/
				"Pow","Divide",/*"Subtract"*/"Log",/*"Exp",*//*"Ceil"*/"Floor","Cosine",/*"Tangent"*/};
				
		private Class type;
		private CommandGene[] commands;
		private CommandGene[][] nodeSets;
		private long startTime; //Used for results output
		
		/**
		 * Set up configuration for genetic program
		 * @param numAttr
		 * @param classifierType
		 * @param testData
		 * @throws InvalidConfigurationException
		 */
	    public GeneticProgram(int numAttr, boolean classifierType, InstanceSet testData)  throws InvalidConfigurationException {
	        

	    	super(new GPConfiguration());

	    	this.numAttr = numAttr;
	    	this.isBinary = classifierType;
	        //Create a new GP configuration
	        GPConfiguration config = getGPConfiguration();
	        initialiseType();
	        initialiseStructure(config,numAttr);
	        initialiseData(numAttr,testData);
	        
	        System.out.println(Arrays.deepToString(this.commands));
	        
	        // Lower fitness value is better as fitness value indicates error rate.
	        config.setGPFitnessEvaluator(new DeltaGPFitnessEvaluator());
	        //Maximum initiation depth
	        config.setMaxInitDepth(this.maximumInitiationDepth);
	        config.setPopulationSize(this.populationSize);
	        config.setMaxCrossoverDepth(this.maxDepth);
	        config.setCrossoverProb((float) this.crossoverProbability);
	        config.setMutationProb((float) this.mutationProbability);
	       
	        if(WRITERESULTS)
	        {
		        this.initResultsFile();
		        //Print results to file after each evolved event
		        config.getEventManager().addEventListener(GeneticEvent.
		                GPGENOTYPE_EVOLVED_EVENT, new GeneticEventListener() {
		              public void geneticEventFired(GeneticEvent event) {
		                GPGenotype genotype = (GPGenotype) event.getSource();
		                int evno = genotype.getGPConfiguration().getGenerationNr();
		                if (evno % 10 == 0) 
		                {
		                	long t2=System.currentTimeMillis();
		                	IGPProgram best = genotype.getAllTimeBest();
	
		                	itfitwriter.println(evno + "\t" + best.getFitnessValue());
		        			timefitwriter.println(evno + "\t" + (t2-startTime)/1000.0);
		                	
	
		                }
		              }
		            });
	        }

	        
	        //Default tournamentSelectorSize is 3. if define as 0 keep default
	        if (this.tournamentSelectorSize > 0) {
	            config.setSelectionMethod(new TournamentSelector(this.tournamentSelectorSize));
	        }

	        //Create a custom fitness function
	        config.setFitnessFunction(new MyFitnessFunction(this.attributes, this.output, this.variables,this.bloatconstant));

	        config.setStrictProgramCreation(true);
	    }

	    /**
	     * If a binary classifier, include boolean commands as well.
	     */
	    private void initialiseType()
	    {
	    	if(this.isBinary)
	    	{
	    		this.type = CommandGene.DoubleClass;
	    		//Concatenate two arrays.
	    		//Allow double version of boolean operators (partly created by myself)
	    		this.finalCommands = Stream.concat(Arrays.stream(this.booleanCommands), Arrays.stream(this.finalCommands))
	                      .toArray(String[]::new);
	    		System.out.println(Arrays.toString(this.finalCommands));
	    	}
	    	else
	    	{	//If not a binary class then only allow boolean operators which will reduce outputs to 0.0 and 1.0
	    		this.type = CommandGene.DoubleClass;
	    	}
	    }
	    /**
	     * Initialize the functions and terminals in the genetic program
	     * @param config
	     * @param numAttr
	     * @throws InvalidConfigurationException
	     */
	    private void initialiseStructure(GPConfiguration config, int numAttr) throws InvalidConfigurationException
	    {
	        this.commands = makeCommands(config, this.finalCommands);
	        // Create the node sets
	        int command_len = commands.length;     
	        this.nodeSets = new CommandGene[1][numAttr+command_len];
	        this.variables = new Variable[numAttr];
	        int variableIndex = 0;
	        for(int i = 0; i < numAttr; i++) 
	        {
	        	Attribute a = Attributes.getAttribute(i);
	            String variableName = (""+a.getName());
                this.variables[variableIndex] = Variable.create(config, variableName, this.type);
                this.nodeSets[0][variableIndex] = this.variables[variableIndex];
                System.out.println("input variable: " + this.variables[variableIndex]);
                variableIndex++;
	        }
	        // assign the functions
	        for(int i = 0; i < command_len; i++) 
	        {
	            this.nodeSets[0][i+numAttr] = this.commands[i];
	        }
	    }
	    
	    /**
	     * Initialize the trainingData to be used in the fitness function
	     * @param numAttr
	     * @param trainingData
	     */
	    private void initialiseData(int numAttr, InstanceSet trainingData)
	    {
	    	Instance[] instances = trainingData.getInstances();
	    	this.attributes = new double[instances.length][numAttr];
	    	this.output = new double[instances.length][1];
	    
			for(int i = 0; i < instances.length; i++ )
			{
    			double[] attrs = instances[i].getRealAttributes();
    			int c = instances[i].getClassValue();
    			if(attrs.length > numAttr)
    			{//truncate array to correct number of input parameters. Perhaps bug in getRealAttributes?
    				attrs = Arrays.copyOf(attrs, attrs.length-(attrs.length - numAttr));
    			}
    			this.attributes[i] = attrs;
    			this.output[i] = new double[]{c};
    			
			}
	    					
	    }
	    
	    /***
	     * Command generator: Inspired from the example SymbolicRegression class
	     * @param functions
	     * @throws InvalidConfigurationException 
	     */
	    private CommandGene[] makeCommands(GPConfiguration conf,String[] functions) throws InvalidConfigurationException
	    {
	    	ArrayList<CommandGene> commandsList = new ArrayList<CommandGene>();
	        int num = functions.length;
            for(int i = 0; i < num; i++) 
            {
            	 switch (functions[i]) 
            	 {
                 case "Multiply": commandsList.add(new Multiply(conf, this.type));
                        break;
                 case "Add":  commandsList.add(new Add(conf, this.type));
                       	break;
                 case "Divide": commandsList.add(new Divide(conf, this.type));
                        break;
                 case "Subtract": commandsList.add(new Subtract(conf, this.type));
                 		break;
                 case "Exp" : commandsList.add(new Exp(conf, this.type));
                 		break;
                 case "Log":  commandsList.add(new Log(conf, this.type));
                 		break;
                 case "Abs":  commandsList.add(new Abs(conf, this.type));
                 		break;
                 case "Pow": commandsList.add(new Pow(conf, this.type));
                     	break;
                 case "Round": commandsList.add(new Round(conf, this.type));
                     	break;
                 case "Ceil": commandsList.add(new Ceil(conf, this.type));
                     	break;
                 case "Floor": commandsList.add(new Floor(conf, this.type));
                     	break;
                 case "Sine": commandsList.add(new Sine(conf, this.type));
                 		break;
                 case "Cosine": commandsList.add(new Cosine(conf, this.type));
                 		break;
                 case "Tangent": commandsList.add(new Tangent(conf, this.type));
                        break;    
                 case "If": commandsList.add(new If(conf, CommandGene.DoubleClass));
                 		break;
                 case "GreaterThanD": commandsList.add(new GreaterThanD(conf));
                 		break;
                 case "LessThanD": commandsList.add(new LessThanD(conf));
          				break;
                 case "AndD": commandsList.add(new AndD(conf));
                 	    break;
            	 case "OrD": commandsList.add(new OrD(conf));
            	 		break;
            	 case "XorD": commandsList.add(new XorD(conf));
            	 		break;
            	 case "NotD": commandsList.add(new NotD(conf));
            	 		break;
            	 case "EqualsD": commandsList.add(new EqualsD(conf));
            	 		break;
            	 case "Sigmoid": commandsList.add(new Sigmoid(conf, CommandGene.DoubleClass));
            	 		break;
            	 case "ModuloD" : commandsList.add(new ModuloD(conf, CommandGene.DoubleClass));		
            	 		break;
                 default: 
                     	break;
            	 }
            	 
            }
            commandsList.add(new Terminal(conf, CommandGene.DoubleClass, 0, 1, true));

            CommandGene[] commands = new CommandGene[commandsList.size()];
            commandsList.toArray(commands);
            return commands;
	    }
	    
	    /**
	     * Initialise a Genotype
	     */
	    @Override
	    public GPGenotype create() throws InvalidConfigurationException {
	    	
	        GPConfiguration config = getGPConfiguration();   
	        // The return type from the genetic program
	        Class[] types = new Class[]{this.type};
	        //Do not need any arguments
	        Class[][] argTypes = { {} };
	        //Create the genotype 
	        GPGenotype result = GPGenotype.randomInitialGenotype(config, types, argTypes,
	                this.nodeSets, this.maximumNumNodes, true);

	        return result;
	    }

	    /**
	     * Compute the result for a set of inputs
	     * @param ind
	     * @param attributes
	     * @return Computed results from genetic program passed
	     */
	    public double computeInstance(final IGPProgram ind, double [] attributes) 
	    {
	    	
	        double result = -1;
	        Object[] noargs = new Object[0]; 
	        for (int i = 0; i < attributes.length; i++) 
	        {
	        		this.variables[i].set(attributes[i]);
	        }
		        try 
		        {     
		            result = ind.execute_double(0, noargs);
		        } 
		        catch (ArithmeticException ex) 
		        {
		            System.out.println(ind);
		            throw ex;
		        }
	        
	        return result;
	   }
	    
	    private void initResultsFile()
		{

			String name = "results_PS_" + this.populationSize + "_CP_" + this.crossoverProbability + 
					"_MP_" + this.mutationProbability + "_MN_" + this.maximumNumNodes + "_BC_" + this.bloatconstant + "_TS_" + this.tournamentSelectorSize;
			for(String a: this.finalCommands)
			{
				String format = "_" + a;
				name += format;
			}
			try {
				this.startTime = System.currentTimeMillis();
				itfitwriter = new PrintWriter(name + ".txt", "UTF-8");
				timefitwriter = new PrintWriter(name + "_time.txt", "UTF-8" );
				solutionwriter = new PrintWriter(name + "_solution.txt", "UTF-8" );
				itfitwriter.println("Iteration\tFitness");
				timefitwriter.println("Iteration\tTime");
			} catch (FileNotFoundException | UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
		}


	}


	


}
