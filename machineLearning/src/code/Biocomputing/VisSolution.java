package Biocomputing;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.*;


import javax.swing.JFrame;

import org.jfree.chart.*;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * Extended from http://stackoverflow.com/questions/13792917/jfreechart-scatter-plot-moving-data-between-series
 * @author Chris Taylor
 * Student Number: 120121386
 */
public class VisSolution extends JFrame {

	private static final long serialVersionUID = 1L;
	private static final int SIZE = 600;
    private static final String title = "Ying-Yang";
    private final XYSeries class1 = new XYSeries("Black");
    private final XYSeries class2 = new XYSeries("White");
    private final XYSeries wrongClass = new XYSeries("Wrong Classification");
    private final XYSeries unclassed = new XYSeries("Unclassed");


    public VisSolution(String s, ClassifierAggregated solution, InstanceSet instances) {
        super(s);
        final ChartPanel chartPanel = createDemoPanelAgg(solution,instances);
        chartPanel.setPreferredSize(new Dimension(SIZE, SIZE));
        this.add(chartPanel, BorderLayout.CENTER);
    }
    
    public VisSolution(String s, Classifier solution, InstanceSet instances) {
        super(s);
        final ChartPanel chartPanel = createDemoPanel(solution,instances);
        chartPanel.setPreferredSize(new Dimension(SIZE, SIZE));
        this.add(chartPanel, BorderLayout.CENTER);
    }

    private ChartPanel createDemoPanelAgg(ClassifierAggregated solution, InstanceSet instances) {
        JFreeChart jfreechart = ChartFactory.createScatterPlot(
            title, "X", "Y", generateDataAgg(solution,instances),
            PlotOrientation.VERTICAL, true, true, false);
        XYPlot xyPlot = (XYPlot) jfreechart.getPlot();
        xyPlot.setDomainCrosshairVisible(true);
        xyPlot.setRangeCrosshairVisible(true);
        XYItemRenderer renderer = xyPlot.getRenderer();
        renderer.setSeriesPaint(0, Color.black);
        renderer.setSeriesPaint(1, Color.blue);
        renderer.setSeriesPaint(2, Color.red);
        renderer.setSeriesPaint(2, Color.orange);


        adjustAxis((NumberAxis) xyPlot.getDomainAxis(), true);
        adjustAxis((NumberAxis) xyPlot.getRangeAxis(), false);
        xyPlot.setBackgroundPaint(Color.white);
        return new ChartPanel(jfreechart);
    }
    private ChartPanel createDemoPanel(Classifier solution, InstanceSet instances) {
        JFreeChart jfreechart = ChartFactory.createScatterPlot(
            title, "X", "Y", generateData(solution,instances),
            PlotOrientation.VERTICAL, true, true, false);
        XYPlot xyPlot = (XYPlot) jfreechart.getPlot();
        xyPlot.setDomainCrosshairVisible(true);
        xyPlot.setRangeCrosshairVisible(true);
        XYItemRenderer renderer = xyPlot.getRenderer();
        renderer.setSeriesPaint(0, Color.black);
        renderer.setSeriesPaint(1, Color.blue);
        renderer.setSeriesPaint(2, Color.red);
        renderer.setSeriesPaint(2, Color.orange);


        adjustAxis((NumberAxis) xyPlot.getDomainAxis(), true);
        adjustAxis((NumberAxis) xyPlot.getRangeAxis(), false);
        xyPlot.setBackgroundPaint(Color.white);
        return new ChartPanel(jfreechart);
    }

    private void adjustAxis(NumberAxis axis, boolean vertical) {
        axis.setRange(-8, 8); //Specific for data set
        axis.setTickUnit(new NumberTickUnit(0.5));
        axis.setVerticalTickLabels(vertical);
    }
    
    private XYDataset generateData(Classifier solution, InstanceSet instanceSet) {
        XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
		Instance []instances = instanceSet.getInstancesOrig();
		
//		Collection<Instance> unclassified = new ArrayList<Instance>();
//		Collection<Instance> wrong = new ArrayList<Instance>();

		int i;
		for(i=0;i<instances.length;i++) 
		{
			int pred = solution.classifyInstance(instances[i]);
			if(pred!=-1) 
			{
				if(instances[i].getClassValue()==pred) 
				{
					if(instances[i].getClassValue() == 0)
					{
			            class1.add(instances[i].getRealAttributes()[0],instances[i].getRealAttributes()[1]);
					}
					else if(instances[i].getClassValue() == 1)
					{
			            class2.add(instances[i].getRealAttributes()[0],instances[i].getRealAttributes()[1]);
					}
				}
				else
				{
					wrongClass.add(instances[i].getRealAttributes()[0],instances[i].getRealAttributes()[1]);
				}
			}
			else
			{
				unclassed.add(instances[i].getRealAttributes()[0],instances[i].getRealAttributes()[1]);
			}
		}
		
        xySeriesCollection.addSeries(class1);
        xySeriesCollection.addSeries(class2);
        xySeriesCollection.addSeries(wrongClass);
        xySeriesCollection.addSeries(unclassed);

        return xySeriesCollection;
    }

    private XYDataset generateDataAgg(ClassifierAggregated solution, InstanceSet instanceSet) {
        XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
		Instance []instances = instanceSet.getInstancesOrig();
		Collection<Instance> unclassified = new ArrayList<Instance>();
		Collection<Instance> wrong = new ArrayList<Instance>();

		int i,j;
		for(i=0;i<instances.length;i++) 
		{
			boolean classified = false;
			boolean correct = false;
			for(j=0;j<solution.classifiers.size();j++) 
			{
				int pred = solution.classifiers.elementAt(j).classifyInstance(instances[i]);
				if(pred!=-1) {
					classified = true;
					if(instances[i].getClassValue()==pred && !correct) {
						correct = true; 
						if(instances[i].getClassValue() == 0)
						{
				            class1.add(instances[i].getRealAttributes()[0],instances[i].getRealAttributes()[1]);
						}
						else if(instances[i].getClassValue() == 1)
						{
				            class2.add(instances[i].getRealAttributes()[0],instances[i].getRealAttributes()[1]);
						}
					}

				}
			}
			if(!classified)
			{
				unclassified.add(instances[i]);
			}
			else if (!correct)
			{
				wrong.add(instances[i]);
			}
		}
		
		for(Instance s: unclassified)
		{
			unclassed.add(s.getRealAttributes()[0],s.getRealAttributes()[1]);
		}
		for(Instance s: wrong)
		{
			wrongClass.add(s.getRealAttributes()[0],s.getRealAttributes()[1]);
		}
        xySeriesCollection.addSeries(class1);
        xySeriesCollection.addSeries(class2);
        xySeriesCollection.addSeries(wrongClass);
        xySeriesCollection.addSeries(unclassed);

        return xySeriesCollection;
    }

    public static void createPlotAgg(ClassifierAggregated subSolution, InstanceSet instances)
    {
                VisSolution demo = new VisSolution(title,subSolution,instances);
                demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                demo.pack();
                demo.setLocationRelativeTo(null);
                demo.setVisible(true);      

    }
    public static void createPlot(Classifier solution, InstanceSet instances)
    {
                VisSolution demo = new VisSolution(title,solution,instances);
                demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                demo.pack();
                demo.setLocationRelativeTo(null);
                demo.setVisible(true);      

    }
}