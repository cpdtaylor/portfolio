/**
 * classifier.java
 *
 */

package Biocomputing;

import java.lang.Math;

abstract public class Classifier {
	
	protected double fitness;

	/**
	 * This function will receive an instance and attempts to predict its class. 
	 * If the classifier can predict its class it will return a class index 
	 * (between 0 and the number of classes in the dataset-1). 
	 * If it cannot predict this instance it will return -1
	 * @param ins
	 * @return
	 */
	public abstract int classifyInstance(Instance ins);
	
	/**
	 * Prints to screen a textual description of the classifier
	 */
	public abstract void printClassifier();
	
	public double getFitness() {
		return fitness;
	}

	
	/**
	 * Compute the fitness on a set of instances 
	 * This function will receive a training set and will compute the fitness of the classifier
	 * 
	 * 
	 * The fitness value is weighted more by error than accuracy
	 * True positive = Classified and classified correctly
	 * True negative = Not classified and class is not correct
	 * False positive = Classified but not classified correctly
	 * Accuracy is ((True positives + True negatives)/Total number of instances)
	 * The error is (True positives/True Positives + False Positives)
	 * You want the error to be as close to 1 as possible
	 * @param is
	 */
	public void computeFitness(InstanceSet is) {
		Instance[] instances = is.getInstances();

		double totalInstances=instances.length;
		double numCorrectPos=0;
		double numCorrectNeg=0;
		double numMatched=0;
		int i;

		for(i=0;i<instances.length;i++) {
			int pred = classifyInstance(instances[i]);
			//if predicted 
			if(pred!= -1) 
			{
				numMatched++;
				if(instances[i].getClassValue()==pred) 
				{
					numCorrectPos++;
				}
			} 
			// Not predicted
			else {
				if(instances[i].getClassValue()!=pred) {
					numCorrectNeg++;
				}
			}
		}

		double accuracy=(numCorrectPos+numCorrectNeg)/totalInstances;
		double error=numCorrectPos/numMatched;

		fitness=accuracy*Math.pow(error,2);
	}

	/**
	 * This function will print to screen a few performance statistics of the classifier given a dataset
	 * accuracy = (Number of TP / (Number of TP + FP))
	 * coverage = (Number of TP + Number of FP) / Total number of instances
	 * @param is
	 */
	public void computeStats(InstanceSet is) {
		Instance[] instances = is.getInstancesOrig();

		double totalInstances=instances.length;
		double numCorrect=0;
		double numMatched=0;
		int i;

		for(i=0;i<instances.length;i++) {
			int pred = classifyInstance(instances[i]);
			if(pred!=-1) {
				numMatched++;
				if(instances[i].getClassValue()==pred) {
					numCorrect++;
				}
			}
		}

		double accuracy=numCorrect/numMatched*100;
		double coverage=numMatched/totalInstances*100;

		System.out.printf("Accuracy %.2f%%, coverage %.2f%%%n",accuracy,coverage);
	}
}
