
------------Running-------------------
To train using a Multilayer Perceptron:

java -cp "code:jars/*" Biocomputing.Control TrainFold0 TestFold0 MLP

To train using Genetic Programming:

java -cp "code:jars/*" Biocomputing.Control TrainFold0 TestFold0 GP
--------------------------------------

-----------Info-----------------------

The MLP code uses the MLP7 configuration shown in the report. I have set the number of iterations to 10000. 

The GP code uses the GP2 configuration shown in the report. I have reduced the number of iterations from 1000 to 200 for demonstration purposes.

Depending on permissions set for jrelog4j_jgap_lf.log an execption may be thrown when running the GP command. This does not prevent the algorithm from successfully running. To stop the error from being generated you can add sudo to the command. 

--------------------------------------


