package Biocomputing;

import org.jgap.*;
import org.jgap.gp.*;
import org.jgap.gp.impl.*;
import org.jgap.util.*;

/**
 * The == operation
 *
 * @author Chris Taylor - experimental 
 * @since 3.5
 */
public class EqualsD
    extends MathCommand implements IMutateable, ICloneable {

  public EqualsD(final GPConfiguration a_conf)
      throws InvalidConfigurationException {
    this(a_conf, CommandGene.DoubleClass);
  }

  public EqualsD(final GPConfiguration a_conf, Class a_returnType)
      throws InvalidConfigurationException {
    super(a_conf, 2, a_returnType);
  }

  public Object clone() {
    try {
      EqualsD result = new EqualsD(getGPConfiguration(), getReturnType());
      return result;
    } catch (Exception ex) {
      throw new CloneException(ex);
    }
  }

  public String toString() {
    return "&1 == &2";
  }

  public String getName() {
    return "EqualsD";
  }

  public double execute_double(ProgramChromosome c, int n, Object[] args) {
    double d1 = c.execute_double(n, 0, args);
    double d2 = c.execute_double(n, 1, args);
    boolean b1 = true;
    boolean b2 = true;
    double epislon = 0.00001;
    if (Math.abs(d1 - d2) < 0.00001) {
      b1 = true;
    }
    else
    {
      b1 = false;
    }
    if (b1) {
      return 1.0d;
    }
    else {
      return 0.0d;
    }
  }

  public CommandGene applyMutation(int index, double a_percentage)
	      throws InvalidConfigurationException {
	    CommandGene mutant;
	    if (a_percentage < 0.5d) {
	      mutant = new NotD(getGPConfiguration());
	    }
	    else {
	      mutant = new EqualsD(getGPConfiguration());
	    }
	    return mutant;
	  }
}
