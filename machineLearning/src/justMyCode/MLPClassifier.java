package Biocomputing;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import org.neuroph.core.Layer;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.Neuron;
import org.neuroph.core.Weight;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.core.learning.IterativeLearning;
import org.neuroph.core.learning.LearningRule;
import org.neuroph.core.learning.SupervisedLearning;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.nnet.learning.ResilientPropagation;
import org.neuroph.util.TransferFunctionType;


/***
 * A Multilayer Perceptron classifier
 * @author Chris Taylor
 * Student Number: 120121386
 */
public class MLPClassifier extends Classifier implements LearningEventListener
{
	
	private final TransferFunctionType ACTIVATIONFUNCTION = TransferFunctionType.TANH  ;
	private final int HIDDENLAYERS[] = {12,8,6,2};
	private final int MAXITTERATIONS = 10000;
	private final double MAXERROR = 0.0001; 
	private final int TRAININGTIME = 100; // If training by time will train untill time up

	private int numAtt;
	private int numClasses;
	private int outputSize;
	private int neurons[];
	private MultiLayerPerceptron solution;
	private double classifierThreshold;
	
	private long startTime; //Use for results in report
	private PrintWriter writer;  //Used for writing results for report
	private final boolean WRITERESULTS = false; // Write results to file?
	
	
	/**
	 * Constructor for the MultiLayer Perceptron. It will take a training set and train the MLP
	 * @param trainingSet
	 * @throws Exception
	 */
	public MLPClassifier(InstanceSet trainingSet)
	{
		int numAtt=Attributes.getNumAttributes();	
		this.numAtt = numAtt;
		int numClasses=Attributes.numClasses;
		this.numClasses = numClasses;
		int outputSize = 0;
		if(WRITERESULTS)
		{
			initResultsFile();
		}
		try
		{
			outputSize = this.getNumberOfOutputs(numClasses);
		}
		catch(IllegalArgumentException e)
		{
			System.out.println(e.getMessage());
			System.exit(1);
		}
			
		this.outputSize = outputSize;
	
		//Initialize the trainingsSet
		DataSet trainingData = this.generateDataSet(trainingSet,numAtt,outputSize);
		
		/*Create a MLP
		 *THE number of perceptrons in the input layer is equal to the number of attribute
		 *1 perceptron in output layer if binary classification otherwise the number of perceptrons is 
		 *equal to the number of classes.
		 */
		this.solution = this.createSolution();
		try 
		{
			//Train the multilayer perceptron using default back-propagation algorithm
			this.trainMLP(trainingData);  
		} catch (InterruptedException e) 
		{
			e.printStackTrace();
			System.exit(2);
		}
		this.trainClassifier(trainingSet); //Train threshold used for classifying instances.
		this.solution.save("MLP_" + this.HIDDENLAYERS.length + ".nnet");
		if(WRITERESULTS)
		{
			this.writer.close(); // writer used for storing results
		}
		

	}
	
	/**
	 * Converts a set of instances into a set of DataRows
	 * @param trainingSet
	 * @param inputSize
	 * @param outputSize
	 * @return
	 */
	private DataSet generateDataSet(InstanceSet trainingSet,int inputSize,int outputSize) 
	{
		Instance[] instances = trainingSet.getInstances();
		DataSet trainingData = new DataSet(inputSize,outputSize);
		for(int i = 0; i < instances.length; i++ )
		{
			//Create a data set row from the instance.
			trainingData.addRow(this.generateRow(instances[i],inputSize));	
		}
		return trainingData;
	}
	
	/**
	 * Converts an instance of type Instance into a DataSetRow.
	 * @param ins
	 * @param inputSize
	 * @return
	 */
	private DataSetRow generateRow(Instance ins,int inputSize)
	{
		double[] attrs = ins.getRealAttributes();
		int c = ins.getClassValue();
		if(attrs.length > inputSize)
		{//truncate array to correct number of input parameters. Perhaps bug in getRealAttributes?
			attrs = Arrays.copyOf(attrs, attrs.length-(attrs.length - inputSize));
		}
		return new DataSetRow(attrs, new double[]{c});
	}
	
	/**
	 * Method to return the number of perceptron needed in the output layer.
	 * @param classCount
	 * @return
	 * @throws Exception
	 */
	private int getNumberOfOutputs(int classCount) throws IllegalArgumentException
	{	
		int outputSize = 0;
		if(classCount <=1)
		{
			throw new IllegalArgumentException("Can not have 1 or less classes in a trainingSet?");
		}
		else if(classCount == 2)
		{
			outputSize = 1; //Only need one perceptron for binary classifier
		}
		else
		{
			outputSize = classCount; //Need a perceptron for each class.
		}
		return outputSize;
	}
	
	/**
	 * Method to train the MLP with a trainingSet.
	 * @param trainingSet
	 * @throws InterruptedException
	 */
	private void trainMLP(DataSet trainingSet) throws InterruptedException
	{
		 SupervisedLearning learningRule = (SupervisedLearning) this.solution.getLearningRule(); 
		 learningRule.setMaxError(this.MAXERROR); 
		 learningRule.setMaxIterations(this.MAXITTERATIONS); // make sure we can end.
		 learningRule.addListener(this);

		 this.solution.learn(trainingSet);
	}
	
	
	/**
	 * Method to train the MLP with a trainingSet.
	 * @param trainingSet
	 * @throws InterruptedException
	 */
	private void trainMLPTL(DataSet trainingSet) throws InterruptedException
	{
		 SupervisedLearning learningRule = (SupervisedLearning) this.solution.getLearningRule(); 
		 learningRule.setMaxError(this.MAXERROR); 
		 learningRule.setMaxIterations(this.MAXITTERATIONS); // make sure we can end.
		 learningRule.addListener(this);
		 long time=System.currentTimeMillis();
		 double runningtime = ((time-time)/1000.0);
		 this.solution.learnInNewThread(trainingSet);
		 while(runningtime < this.TRAININGTIME)
		 {
			 long newtime=System.currentTimeMillis();
			 runningtime = ((newtime-time)/1000.0);
			//wait 
		 }
		 this.solution.stopLearning();
	}
	
	
	/**
	 * Create a MultiLayer Perceptron
	 * @return
	 */
	private MultiLayerPerceptron createSolution()
	{
		this.neurons = new int[this.HIDDENLAYERS.length + 2]; // plus 2 for input and output layers
		//Add input layer
		this.neurons[0] =  this.numAtt;
		//Add hidden layers
		for(int i = 0; i < this.HIDDENLAYERS.length; i++)
		{
			neurons[i+1] = this.HIDDENLAYERS[i];
		}
		//Add output layer
		neurons[neurons.length-1] = this.outputSize;
		MultiLayerPerceptron sol = new MultiLayerPerceptron(this.ACTIVATIONFUNCTION,neurons);
//		BackPropagation bp = new BackPropagation();
//		bp.setLearningRate(0.02);
//		sol.setLearningRule(bp);
		sol.setLearningRule(new ResilientPropagation()); //Resilient is the most stable
		return sol;
	}
	
	/**
	 * Function to test the NeuralNetwork. 
	 * @param instances
	 */
    private void testNeuralNetwork(InstanceSet instances) 
    {
    	DataSet testdata = this.generateDataSet(instances, this.numAtt, this.outputSize);
        for(DataSetRow dataRow : testdata.getRows()) 
        {
            this.solution.setInput(dataRow.getInput());
            this.solution.calculate();
            double[] networkOutput = this.solution.getOutput();
            System.out.print("Input: " + Arrays.toString(dataRow.getInput()) );
            System.out.print("  Output: " + Arrays.toString(networkOutput) ); 
            System.out.println("  Desired Output: " + Arrays.toString(dataRow.getDesiredOutput())); 
        }

    }
    
    /**
     * Train the threshold used for classifying instances
     */
    private void trainClassifier(InstanceSet is)
    {
    	double step = 0.05;
    	double accuracy = 0.0;
    	double thresholdToUse=0.5;
    	this.classifierThreshold = 0;
    	
    	while(this.classifierThreshold <= 1)
    	{
    		double a = this.getAccuracy(is);
    		if(a > accuracy)
    		{
    			accuracy = a;
    			thresholdToUse = this.classifierThreshold;
    		}
    		this.classifierThreshold += step;
    	}
    	thresholdToUse = Math.round(thresholdToUse * 100);
    	thresholdToUse = thresholdToUse/100;
    	this.classifierThreshold = thresholdToUse;
    	
    }
    
    /**
     * Used for getting the accuracy of a threshold
     * @param is
     * @return
     */
    private double getAccuracy(InstanceSet is)
    {
    	Instance[] instances = is.getInstancesOrig();

		double totalInstances=instances.length;
		double numCorrect=0;
		double numMatched=0;
		int i;

		for(i=0;i<instances.length;i++) {
			int pred = classifyInstance(instances[i]);
			if(pred!=-1) {
				numMatched++;
				if(instances[i].getClassValue()==pred) {
					numCorrect++;
				}
			}
		}

		double accuracy=numCorrect/numMatched*100;
    	return accuracy;
    	
    }
	
	@Override
	public int classifyInstance(Instance ins) 
	{
		int c = -1;
		DataSetRow dsr = this.generateRow(ins, this.numAtt);
		this.solution.setInput(dsr.getInput());
        this.solution.calculate();
        double[] networkOutput = this.solution.getOutput();
		if(this.numClasses == 2)
		{
			//binary classifier
			double output = Math.abs(networkOutput[0]);
			int retval = Double.compare(output, this.classifierThreshold);
			
			if(retval > 0) 
			{
				c = 1;
			}
			else if(retval <= 0) // A MLP will *always classify an instance. 
			{
				c = 0;
			}			
		}
		return c;
	}

	@Override
	public void printClassifier() 
	{
		System.out.println("Activation function used: " + this.ACTIVATIONFUNCTION.getTypeLabel() + "\n");
		System.out.println("Number of neurons in Input Layer: " + this.numAtt);
		for(int i = 0; i< this.HIDDENLAYERS.length; i++)
		{
			System.out.println("Number of neurons in Hidden Layer" + (i+1) + ": " + this.HIDDENLAYERS[i]);
		}
		System.out.println("Number of neurons in Output Layer: " + this.outputSize);
		Layer[] layers = this.solution.getLayers();

		//Print the hidden layer weights
		for(int i = 1; i < layers.length -1; i++)
		{
			Neuron[] neurons = layers[i].getNeurons();
			System.out.println("\n----------------Hidden Layer " + (i) + " Neurons----------------\n");
			for(int j = 0; j<neurons.length -1; j++)
			{
				Weight[] weights = neurons[j].getWeights();
				System.out.print("Neuron " + (j+1) + " weights: ");
				for(Weight w : weights)
				{
					System.out.print(w.value + ", ");
				}
				System.out.println("");
			}
			System.out.println("");
		}
		
		System.out.println("\nThe trained threshold for classifying instances is: " + this.classifierThreshold);
	}

	private void initResultsFile()
	{
		//long millis = System.currentTimeMillis();
		//String name = "results_" + "iterations_" + this.MAXITTERATIONS;
		String name = "results_" + "trainingtime_" + this.TRAININGTIME + "_" + this.ACTIVATIONFUNCTION.getTypeLabel();
		for(int a: this.HIDDENLAYERS)
		{
			String format = "_" + a;
			name += format;
		}
		this.startTime = System.currentTimeMillis();
		try {
			this.writer = new PrintWriter(name + ".txt", "UTF-8");
			//writer.println("Iteration\tError");
			writer.println("Time\tError");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void handleLearningEvent(LearningEvent event) {
		SupervisedLearning rule = (SupervisedLearning)event.getSource();
		int iteration = rule.getCurrentIteration();
		System.out.println( "Training, Iteration" + rule.getCurrentIteration() + ", Error:" + rule.getTotalNetworkError());
		if (iteration % 200 == 0)
		{
			long t2=System.currentTimeMillis();
			//this.writer.println(rule.getCurrentIteration() + "\t" + rule.getTotalNetworkError() );
			if(this.WRITERESULTS)
			{
				this.writer.println((t2-this.startTime)/1000.0 + "\t" + rule.getTotalNetworkError() );
			}
		}
	}

}
