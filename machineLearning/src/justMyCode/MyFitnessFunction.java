package Biocomputing;

import org.jgap.gp.GPFitnessFunction;
import org.jgap.gp.IGPProgram;
import org.jgap.gp.impl.ProgramChromosome;
import org.jgap.gp.terminal.Variable;

/**
 * A fitness function for my genetic program 
 * @author Chris Taylor
 * Student Number: 120121386
 *
 */
public class MyFitnessFunction extends GPFitnessFunction {

	private double[][] attributes;
	private double[][] output;
	private Variable[] variables;
	private double bloatconstant;

    private static Object[] NO_ARGS = new Object[0];
    
    public MyFitnessFunction(double[][] attributes,double[][] output,Variable[] variables,double bloatconstant)
    {
    	this.attributes = attributes;
    	this.output=output;
    	this.variables=variables;
    	this.bloatconstant = bloatconstant;
    }

    @Override
    protected double evaluate(final IGPProgram program) {
    	
        double result = 0.0;
        double addedResults = 0;
        double value;
        ProgramChromosome chrom = program.getChromosome(0);
        int numTerms =  chrom.numFunctions()  + chrom.numTerminals();
        for (int i = 0; i < this.attributes.length; i++) {
        	
        	for(int j = 0; j<this.attributes[i].length; j++)
        	{
        		this.variables[j].set(this.attributes[i][j]);
        	}
                 value = program.execute_double(0, NO_ARGS);
            
            // The closer longResult gets to 0 the better the algorithm.
            addedResults += Math.abs(value - this.output[i][0]);
        }

        result = addedResults;
        result += (this.bloatconstant*numTerms); //control bloat;
        return result;
    }

}