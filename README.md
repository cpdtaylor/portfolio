#Software Portfolio
This repository contains a few chosen examples of my work.
## [/dissertation] Dissertation
This contains the dissertation written alongside the development of the Drogon Web Application (DWA). It focuses on the technologies that enable DWA to scale its workload and how it was built. 
## [/machineLearning] Machine Learning University Project
This project focused on comparing two machine learning algorithms for the classification of a Ying-Yang symbol. I implemented a Multilayer Perceptron Neural Network algorithm and Genetic Programming algorithm in Java and then compared their performance.
** Keywords: **
Java, Neural Network, Genetic Programming
## [/swiftMobileApp] Swift Mobile Application Development University Project
This project involved designing and developing a mobile app for iOS.
** Keywords: **
Mobile, Swift, PHP
## [/cryptography] Cryptography University Projects
/Extended-Euclidian_DiffieHellman contains coursework that involved implementing the Extended-Euclidian and Diffie Hellman algorithms in Java.
/SystemSecurity contains coursework for implementing a file system security feature using Java cryptography libraries.
## [/PHPUniversityModule] PHP Web Project at University
This directory contains a University Web project that was built using a custom PHP framework. The framework was created by my Lecturer.
** Keywords: **
PHP, Twig, RedBean, Framework 
