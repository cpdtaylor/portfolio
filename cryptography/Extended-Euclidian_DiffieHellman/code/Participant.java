

import java.math.BigInteger;

/**
 * Abstract class to define methods needed to be implemented by participants in the Diffie-Hellman
 * key-exchange. Currently this can changed to an Interface.
 * @author Chris Taylor
 * @Studentnumber: 120121386
 */
public abstract class Participant {

	public abstract BigInteger getSecret();
	
}
