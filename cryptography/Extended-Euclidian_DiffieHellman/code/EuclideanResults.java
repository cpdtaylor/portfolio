

import java.math.BigInteger;

/**
 * Class to store the results of the Extended Euclidian algorithm as specified in the coursework
 * @author Chris Taylor
 * @Studentnumber: 120121386
 *
 */
public class EuclideanResults 
{
	private BigInteger gcd;
	private BigInteger s;
	private BigInteger t;
	
	public EuclideanResults(BigInteger gcd, BigInteger s, BigInteger t)
	{
		this.gcd = gcd;
		this.s = s;
		this.t = t;
		
	}

	public BigInteger getGCD() {
		return gcd;
	}

	public void setGCD(BigInteger gcd) {
		this.gcd = gcd;
	}

	public BigInteger getS() {
		return s;
	}

	public void setS(BigInteger s) {
		this.s = s;
	}

	public BigInteger getT() {
		return t;
	}

	public void setT(BigInteger t) {
		this.t = t;
	}
	
	
}
