

import java.math.BigInteger;

/**
 * Class to store messages sent from Alice
 * @author Chris Taylor
 * @Studentnumber: 120121386
 */
public class Message1 {

	private BigInteger modulus;
	private BigInteger base;
	private BigInteger valueA;
	
	public Message1(BigInteger modulus, BigInteger base, BigInteger valueA) {
		this.modulus = modulus;
		this.base = base;
		this.valueA = valueA;
	}

	public BigInteger getModulus() {
		return modulus;
	}

	public void setModulus(BigInteger modulus) {
		this.modulus = modulus;
	}

	public BigInteger getBase() {
		return base;
	}

	public void setBase(BigInteger base) {
		this.base = base;
	}

	public BigInteger getValueA() {
		return valueA;
	}

	public void setValueA(BigInteger valueA) {
		this.valueA = valueA;
	}
	
	
	
}
