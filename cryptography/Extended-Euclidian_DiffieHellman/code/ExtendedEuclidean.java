

import java.math.BigInteger;

/**
 * Class to implement Extended-Euclidian algorithm and linear equation solvers. 
 * Also run those algorithms on example data in question 1 and 2
 * @author Chris Taylor
 * @Studentnumber: 120121386
 */
public class ExtendedEuclidean {
		
		private final BigInteger N2A =
				new BigInteger("643808006803554439230129854961492699151386107534013432918073439524138264842370630061369715394739134090922937332590384720397133335969549256322620979036686633213903952966175107096769180017646161851573147596390153");
		private final BigInteger a2A =
        		new BigInteger("34325464564574564564768795534569998743457687643234566579654234676796634378768434237897634345765879087764242354365767869780876543424");
		private final BigInteger b2A =
				new BigInteger("45292384209127917243621242398573220935835723464332452353464376432246757234546765745246457656354765878442547568543334677652352657235");
				
		private final BigInteger N2B =
				new BigInteger("342487235325934582350235837853456029394235268328294285895982432387582570234238487625923289526382379523573265963293293839298595072093573204293092705623485273893582930285732889238492377364284728834632342522323422");
		private final BigInteger a2B =
				new BigInteger("34325464564574564564768795534569998743457687643234566579654234676796634378768434237897634345765879087764242354365767869780876543424");
		private final BigInteger b2B =
				new BigInteger("24243252873562935279236582385723952735639239823957923562835832582635283562852252525256882909285959238420940257295265329820035324646");
		
        
        private final int certainty = 500; //Certainty for testing if prime
        
	    /***
	     * Function to perform the Extended Euclidean algorithm
	     * @param x
	     * @param y
	     * @return Obtain d = xs + yt in an EuclideanResults object
	     */
	    public EuclideanResults performExtendedEuclidean(BigInteger x, BigInteger y)
	    {
	        BigInteger s = BigInteger.valueOf(0); 
	        BigInteger t = BigInteger.valueOf(1);
	        BigInteger xcoef = BigInteger.valueOf(1);
	        BigInteger ycoef = BigInteger.valueOf(0);
	        BigInteger temp; //Store temp values here
	        BigInteger previousR = BigInteger.valueOf(-1); //Store the last remainder here. When r = 0, previoursR is equal to the GCD
	        while (!y.equals(BigInteger.valueOf(0)))
	        {
	        	previousR = y;
	            BigInteger q = x.divide(y); //calculate quotient
	            BigInteger r = x.mod(y); //calculate remainder
	            
	            x = y;
	            temp = s;
	            s = xcoef.subtract((q.multiply(s)));
	            xcoef = temp;
	            y = r;
	            temp = t;
	            t = ycoef.subtract((q.multiply(t)));
	            ycoef = temp;            
	        }
	        
	        EuclideanResults results = new EuclideanResults(previousR,xcoef,ycoef);
	        
	        return results;
	    }
	   
	    /**
	     * Uses Extended Euclidian algorithm to compute the value for x
	     * Given ax + b = 0 in ZN solve for x
	     * @param a
	     * @param b
	     * @param N
	     * @return The value of x if it can be computed, null if it can't.
	     */
	    public BigInteger solveLinearWithEuclidian(BigInteger a, BigInteger b, BigInteger N)
	    {
	    	BigInteger x;
	    	b = b.negate();  //negating b    ax = -b MOD N
	        long startTime = System.currentTimeMillis();
	    	EuclideanResults results = this.performExtendedEuclidean(a,N);
	    	BigInteger gcd = results.getGCD();
	    	if(gcd.equals(BigInteger.ONE))
	    	{
	    		//a has a multiplicative inverse Modulo N if and only if gcd(a,N) = 1
	    		//If a 'has' a multiplicative inverse then this equation can be solved for x
	    		//x = a(^-1)(-b) mod N
	    		//1=a(s) + N(t)
	    		//1 = a(s) mod N
	    		BigInteger s = results.getS();
	    		BigInteger inverse = s.mod(N); //Calculate s in ZN. = s'
	    		//a^-1 = s'
	    		x = (inverse.multiply(b)).mod(N); //x = s'(-b) mod N
		        long endTime   = System.currentTimeMillis();
		        long totalTime = endTime - startTime;
		        System.out.println("Extended Euclidian algorithm TIME: " + totalTime);
	    		return x;
	    	}
	    	else
	    	{
		    	return null; // equation is unsolvable.
	    	}

	    }
	    
	    /***
	     * Uses BigInteger's 'modInverse' method to solve the linear equation
	     * Given ax + b = 0 in ZN solve for x
	     * @param a
	     * @param b
	     * @param N
	     * @return The value of x if it can be computed, null if it can't.
	     */
	    public BigInteger solveLinearWithBI(BigInteger a, BigInteger b, BigInteger N)
	    {
	    	BigInteger x;
	    	b = b.negate();  //negating b    ax = -b MOD N
	    	try
	    	{
		        long startTime = System.currentTimeMillis();
	    		BigInteger inverse = a.modInverse(N);
	    		x = (inverse.multiply(b)).mod(N); //x = s'(-b) mod N
		        long endTime   = System.currentTimeMillis();
		        long totalTime = endTime - startTime;
		        System.out.println("BigInteger.modInverse TIME: " + totalTime);
	    		return x;
	    	}
	    	catch(ArithmeticException e)
	    	{
	    		//Can't compute x
	    		// m ≤ 0, or this BigInteger has no multiplicative inverse mod m (that is, this BigInteger is not relatively prime to m).
		    	return null;	
	    	}

	    }
	    
	    /**
	     * Uses Fermat's Theorem method to solve the linear equation
	     * In Zp the inverse of a: a^-1 = a^p-2(mod p)
	     * Therefore Fermats therorem requires that N is a prime number
	     * Given ax + b = 0 in ZN solve for x
	     * @param a
	     * @param b
	     * @param N
	     * @return The value of x if it can be computed, null if it can't.
	     */
	    public BigInteger solveLinearWithFT(BigInteger a, BigInteger b, BigInteger N)
	    {
	    	BigInteger x;
	    	b = b.negate();  //negating b    ax = -b MOD N
	    	if(N.isProbablePrime(this.certainty))
	    	{
		        long startTime = System.currentTimeMillis();
	    		//a^-1 = a^N-2 mod N if N is prime
	    		BigInteger inverse = a.modPow(N.subtract(BigInteger.valueOf(2)),N);
	    		x = (inverse.multiply(b)).mod(N); //x = s'(-b) mod N
		        long endTime   = System.currentTimeMillis();
		        long totalTime = endTime - startTime;
		        System.out.println("Fermat Therom TIME: " + totalTime);
		        
	    		return x;
	    	}
	    	else
	    	{
	    		return null;
	    	}
	    }
	    
	    /**
	     * Run question 1
	     */
	    public void question1()
	    {
	    	BigInteger x = new BigInteger("1572855870797393");
	        BigInteger y = new BigInteger("630065648824575");
	        EuclideanResults results = this.performExtendedEuclidean(x, y);       
	        System.out.println("d:" + results.getGCD() + "\ts:" + results.getS() + "\tt:" + results.getT());
	        System.out.println("d=xs+yt");
	        System.out.println(results.getGCD() + "=" + x + "(" + results.getS() + ")+" + y + "(" + results.getT() + ")");	
	    }
	    
	    
	    /**
	     * Run question 2a using the Extended Euclidian Algorithm
	     */
	    public void question2AEEA()
	    {		      
		      BigInteger result = this.solveLinearWithEuclidian(this.a2A, this.b2A, this.N2A);
		      this.printResult(result);
	    }
	    
	    /**
	     * Run question 2a using Fermat's Theorem
	     */
	    public void question2AFT()
	    {
		      BigInteger result = this.solveLinearWithFT(this.a2A, this.b2A, this.N2A);
		      this.printResult(result);
	    }
	    
	    /**
	     * Run question 2a using BigInteger modInverse
	     */
	    public void question2ABI()
	    {		      
		      BigInteger result = this.solveLinearWithBI(this.a2A, this.b2A, this.N2A);
		      this.printResult(result);
	    }
	    
	    /**
	     * Run question 2b using Extended Euclidian Algorithm
	     */
	    public void question2BEEA()
	    {
		      BigInteger result = this.solveLinearWithEuclidian(this.a2B, this.b2B, this.N2B);
		      this.printResult(result);
	    }
	    
	    /**
	     * Run question 2b using BigInteger modInverse
	     */
	    public void question2BBI()
	    {
		      BigInteger result = this.solveLinearWithBI(this.a2B, this.b2B, this.N2B);
		      this.printResult(result);
	    }
	    
	    /**
	     * Print if the numbers used in questions 2 and 3 are prime
	     */
	    public void isPrime()
	    {
	        System.out.println("N is a prime in 2a: " + this.N2A.isProbablePrime(this.certainty));
	        System.out.println("N is a prime in 2b: " + this.N2B.isProbablePrime(this.certainty));
	        System.out.println("'a' is a prime in 2a: " + this.a2A.isProbablePrime(this.certainty));
	        System.out.println("'a' is a prime in 2b: " + this.a2B.isProbablePrime(this.certainty));

	    }
	    
	    /**
	     * Print result from linear equation algorithm
	     * If the result from the linear equations solvers is not null then print the result
	     * @param result
	     */
	    private void printResult(BigInteger result)
	    {
	        if(result != null)
	        {
	        	//Successful Result
	        	System.out.println("The value of x is: " + result);
	        }
	        else
	        {	
	        	//Unsuccessful
	        	System.out.println("The value of x could not be computed");
	        }
	    }
	        
	    public static void main (String[] args) 
	    {
	        ExtendedEuclidean ee = new ExtendedEuclidean();
	           
	        // EX1 Question1
	        System.out.println("-------------------------------Question1-----------------------------------\n");
	        ee.question1();
	        System.out.println("---------------------------------------------------------------------------\n");
	        
	        //EX2 Question 2a
	        System.out.println("-------------------------------Question2a-----------------------------------\n");
	        
	        //EEA
	        ee.question2AEEA();
	        System.out.println("");
	        
	        //BI modular inverse
	        ee.question2ABI();
	        System.out.println("");

	        //Fermat's Theorem
	        ee.question2AFT();
	        System.out.println("");
	        System.out.println("---------------------------------------------------------------------------\n");

	        
	        //EX2 Question 2b
	        System.out.println("-------------------------------Question2b-----------------------------------\n");
	        ee.question2BEEA();
	        ee.question2BBI();
	        System.out.println("---------------------------------------------------------------------------\n");
	        
	        //Question3 - discover if N is prime for 2a and 2b
	        ee.isPrime();


	    }
	
	
}
