

import java.math.BigInteger;

/**
 * Class that implements methods for an attack from Eve.
 * @author Chris Taylor
 * @Studentnumber: 120121386
 *
 */
public class Eve extends Participant{
	
	private BigInteger modulus;
	private BigInteger generator;
	private BigInteger e;
	private BigInteger keyA;
	private BigInteger keyB;
	
	/**
	 * Intercept a message and replace it
	 * @param msg1
	 * @return
	 */
	public Message1 computeMessageEtoB(Message1 msg1)
	{
		this.e = DiffieHellman.getRandValue(msg1.getModulus());
		this.modulus = msg1.getModulus();
		this.generator = msg1.getBase();
		BigInteger valueE = msg1.getBase().modPow(this.e, msg1.getModulus());
		this.computeEA(msg1);
		return new Message1(msg1.getModulus(),msg1.getBase(),valueE);
	}
	
	/**
	 * Compute the session key between alice and eve
	 * @param msg1
	 */
	private void computeEA(Message1 msg1)
	{
		this.keyA = msg1.getValueA().modPow(this.e, msg1.getModulus());
	}
	
	/**
	 * Compute the session key between bob and eve
	 * @param msg2
	 */
	private void computeEB(BigInteger msg2)
	{
		this.keyB = msg2.modPow(this.e, this.modulus);
	}
	
	/**
	 * Intercept a message from bob
	 * @param msg2
	 * @return
	 */
	public BigInteger computeMessageEtoA(BigInteger msg2)
	{
		this.computeEB(msg2);
		return this.generator.modPow(this.e, this.modulus);
	}
	
	public BigInteger getKeyEA()
	{
		return this.keyA; 
	}
	
	public BigInteger getKeyEB()
	{
		return this.keyB;
	}
	
	/**
	 * Conceptual in order to print the secret.
	 */
	public BigInteger getSecret()
	{
		return this.e;
	}

}
