

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * A class to implement methods needed for Diffie-Hellman key exchange
 * @author Chris Taylor
 * @Studentnumber: 120121386
 */
public class DiffieHellman {
	
	private static final int certainty = 1000; //Certainty for testing prime numbers
	
	/**
	 * Get a prime number with the given bitlength
	 * @param bitLength
	 * @return
	 */
	public static BigInteger getPrime(int bitLength)
	{
		SecureRandom r = new SecureRandom();
		BigInteger bi = BigInteger.probablePrime(bitLength, r);
		if(bi.isProbablePrime(certainty))
		{
			//High Probability that the BigInteger is indeed a prime number
			return bi;
		}
		else
		{
			return null;
		}
	}
	
	/**
	 * Get a generator g in {2-p-1)
	 * @param modulus
	 * @return
	 */
	public static BigInteger getRandGenerator(BigInteger modulus)
	{
	    BigInteger min = new BigInteger("2"); //2 in spec
	    return getRand(modulus,min);
	}
	
	/**
	 * Get a rand value in {1-p-1} as specified in lecture notes.
	 * @param modulus
	 * @return
	 */
	public static BigInteger getRandValue(BigInteger modulus)
	{

	    BigInteger min = new BigInteger("1"); 
	    return getRand(modulus,min);
		
	}
	
	/**
	 * Method to get a rand number between two specified ranges
	 * @param modulus
	 * @param min
	 * @return
	 */
	private static BigInteger getRand(BigInteger modulus, BigInteger min)
	{
		SecureRandom sr = new SecureRandom();
	    BigInteger value = new BigInteger(modulus.bitLength(), sr);
		while(value.compareTo(modulus) >= 0 || value.compareTo(min) < 0) 
		{
		        value = new BigInteger(modulus.bitLength(), sr);
		}
		return value;
	}
			

}

