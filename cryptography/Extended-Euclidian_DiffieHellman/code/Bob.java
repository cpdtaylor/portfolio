

import java.math.BigInteger;

/**
 * Implements methods for Bob in Diffie-Hellman Exchange
 * @author Chris Taylor
 * @Studentnumber: 120121386
 */
public class Bob extends Participant {

	private BigInteger b;
	
	/**
	 * Send value B to Alice
	 * @param msg1
	 * @return
	 */
	public BigInteger computeMessageBtoA(Message1 msg1)
	{
		this.b = DiffieHellman.getRandValue(msg1.getModulus());
		BigInteger valueB = msg1.getBase().modPow(this.b, msg1.getModulus());
		return valueB;
	}
	
	/**
	 * Compute the session key, keyB
	 * @param msg1
	 * @return
	 */
	public BigInteger computeKeyB(Message1 msg1)
	{
		return msg1.getValueA().modPow(this.b, msg1.getModulus());
	}
	
	/**
	 * Conceptual in order to print the secret.
	 */
	public BigInteger getSecret()
	{
		return this.b;
	}
	
	
}
