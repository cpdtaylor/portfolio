

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

/**
 * Class to run an example of a Diffie-Hellman key-exchange
 * @author Chris Taylor
 * @Studentnumber: 120121386
 */
public class Exercise3 {

	private static final int MODSIZE = 1024; 
	
	private static final int RUNQUESTION2 = 0;
	private static final int RUNQUESTION4 = 1;
	
	/**
	 * Question 2
	 */
	private static void printTranscriptq2(Alice alice, Bob bob, Message1 msg1, BigInteger msg2,BigInteger keyA,BigInteger keyB,String filename) throws FileNotFoundException, UnsupportedEncodingException
	{
		System.out.println("secretA = " + alice.getSecret());
		System.out.println("secretB = " + bob.getSecret());
		System.out.println("msg1.modulus = " + msg1.getModulus());
		System.out.println("msg1.base = " + msg1.getBase());
		System.out.println("msg1.valueA = " + msg1.getValueA());
		System.out.println("msg2.valueB = " + msg2);
		System.out.println("keyA = " + keyA);
		System.out.println("keyB = " + keyB);
		
		PrintWriter writer = new PrintWriter(filename, "UTF-8"); 
		writer.println("secretA = " + alice.getSecret());
		writer.println("secretB = " + bob.getSecret());
		writer.println("msg1.modulus = " + msg1.getModulus());
		writer.println("msg1.base = " + msg1.getBase());
		writer.println("msg1.valueA = " + msg1.getValueA());
		writer.println("msg2.valueB = " + msg2);
		writer.println("keyA = " + keyA);
		writer.println("keyB = " + keyB);
		writer.close();
	}
	/**
	 * Question 4
	 */
	private static void printTranscriptq4(Alice alice, Bob bob,Eve eve, Message1 msg1, BigInteger msg2,Message1 valueE,BigInteger keyA,BigInteger keyB,String filename) throws FileNotFoundException, UnsupportedEncodingException
	{
		System.out.println("secretA = " + alice.getSecret());
		System.out.println("secretB = " + bob.getSecret());
		System.out.println("secretE = " + eve.getSecret());
		System.out.println("msg1.modulus = " + msg1.getModulus());
		System.out.println("msg1.base = " + msg1.getBase());
		System.out.println("msg1.valueA = " + msg1.getValueA());
		System.out.println("msg2.valueB = " + msg2);
		System.out.println("eve.valueE = " + valueE.getValueA());
		System.out.println("keyA = " + keyA);
		System.out.println("keyB = " + keyB);
		System.out.println("keyEA = " + eve.getKeyEA());
		System.out.println("keyEB = " + eve.getKeyEB());

		PrintWriter writer = new PrintWriter(filename, "UTF-8"); 
		writer.println("secretA = " + alice.getSecret());
		writer.println("secretB = " + bob.getSecret());
		writer.println("secretE = " + eve.getSecret());
		writer.println("msg1.modulus = " + msg1.getModulus());
		writer.println("msg1.base = " + msg1.getBase());
		writer.println("msg1.valueA = " + msg1.getValueA());
		writer.println("msg2.valueB = " + msg2);
		writer.println("eve.valueE = " + valueE.getValueA());
		writer.println("keyA = " + keyA);
		writer.println("keyB = " + keyB);
		writer.println("keyEA = " + eve.getKeyEA());
		writer.println("keyEB = " + eve.getKeyEB());
		writer.close();
	}
	
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException
	{
		int run = RUNQUESTION4;
		
		//Generate public mod N and public generator g
		BigInteger n = DiffieHellman.getPrime(MODSIZE);
		BigInteger g = DiffieHellman.getRandGenerator(n);
	
		
		if(run == RUNQUESTION2)
		{
			/****Question2****/
			Message1 msg1;
			BigInteger msg2;
			BigInteger keyA;
			BigInteger keyB;
			
			Alice alice = new Alice(n,g);
			Bob bob = new Bob();
			msg1 = alice.computeMessageAtoB();
			msg2 = bob.computeMessageBtoA(msg1);
			keyA = alice.computeKeyA(msg2);
			keyB = bob.computeKeyB(msg1);
			printTranscriptq2(alice,bob,msg1,msg2,keyA,keyB,"transcriptq2.txt");
		}
		else if(run == RUNQUESTION4)
		{
			/****Question4****/
			Message1 msg1;
			BigInteger msg2;
			BigInteger keyA;
			BigInteger keyB;
			
			Message1 valueA;
			BigInteger valueB;
			Message1 valueE;
			
			Alice alice = new Alice(n,g);
			Bob bob = new Bob();
			Eve eve = new Eve();
			
			msg1 = alice.computeMessageAtoB();
			valueA = msg1;
			msg1 = eve.computeMessageEtoB(msg1); //eve intercepts and replaces message
			valueE = msg1;
			msg2 = bob.computeMessageBtoA(msg1);
			valueB = msg2;
			msg2 = eve.computeMessageEtoA(msg2); //eve intercepts and replaces message
			keyA = alice.computeKeyA(msg2); 
			keyB = bob.computeKeyB(msg1);

			printTranscriptq4(alice,bob,eve,valueA,valueB,valueE,keyA,keyB,"transcriptq4.txt");
		}
		
		
		
	}
}
