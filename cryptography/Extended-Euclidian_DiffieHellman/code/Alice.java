

import java.math.BigInteger;

/**
 * Implements methods for Alice in Diffie-Hellman Exchange
 * @author Chris Taylor
 * @Studentnumber: 120121386
 */
public class Alice extends Participant{

	private BigInteger modulus;
	private BigInteger generator;
	private BigInteger a;
	
	/**
	 * Takes public known values modulus and generator
	 * @param modulus
	 * @param g
	 */
	public Alice(BigInteger modulus, BigInteger g)
	{
		this.modulus = modulus;
		this.generator =  g;
		this.a = DiffieHellman.getRandValue(modulus);
	}

	/**
	 * Compute valueA to be sent to B
	 * @return
	 */
	public Message1 computeMessageAtoB()
	{
		BigInteger valueA = this.generator.modPow(this.a,this.modulus);
		return new Message1(this.modulus,this.generator,valueA);
	}
	
	/**
	 * Compute the session key keyA
	 * @param valueB
	 * @return
	 */
	public BigInteger computeKeyA(BigInteger valueB)
	{
		return valueB.modPow(this.a, this.modulus);
	}
	
	/**
	 * Conceptual in order to print the secret.
	 */
	public BigInteger getSecret()
	{
		return this.a;
	}

}
