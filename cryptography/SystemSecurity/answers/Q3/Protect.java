import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;




/**
 * Protect class for Question 3
 * 
 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
 * Student Number: 120121386
 */
public class Protect {
	
	/*************** Messages **************/
	private static String successMessage = "Succesfully Completed!";
	private static String wrongPassMessage = "Wrong Password!";
	private static String noFileMessage = "File name supplied does not exist in our records";
	private static String correctSigMsg = "Correct Signiture!";
	private static String wrongSigMsg = "Wrong Signiture!";
	private static String noPermission = "You do not have the correct permissions for this operation";
	
	private static String noFileEncMsg = "There is not a file in the file system to encrypt";
	private static String noFileDecMsg = "There is not a file in the file system to decrypt";
	private static String noSigFileMsg = "There is not a signature file for the specified file to decrypt";
	
	private static String notEncryptedFileExists = "An unecrypted version of %s exists. Deleting it";
	private static String missingFile = "%s is missing!";
	private static String unwantedExtraFile = "%s is an unwanted extra file. Deleting it";
	private static String unwantedExtraDirectory = "Deleting additional directory %s";
	private static String sigNotCorrectDelete = "Signature for %s is not correct. Removing %s and %s";
	private static String sigFileDoesNotExistDelete = "Signature for %s does not exist. Removing %s";
	private static String pkFileDoesNotExistDelete = "Public key for %s does not exist. Removing %s";

	/*************** Settings **************/

	private static String encryptionExtention = ".enc";
	private static String signatureExtention = ".sig";

	private static final String DESTROYROLENAME = "EM";

	private static String folder = "./";
	
	private static final String ENCRYPTFLAG = "-e";
	private static final String DECRYPTFLAG = "-d";
	private static final String CHECKFLAG = "-c";
	private static final String DESTROYFLAG = "-b";


	public static void main(String[] args) throws Exception
	{
				
		int length = args.length;
		try
		{
			if(length > 0)
			{
				String flag = args[0];
				
				Map<String,Role> mroles = Files.getRoles();

				if(length == 3)
				{
					String filename = args[1];
					String password = args[2];
		
					//Check the password
					Role r = Files.checkPassword(mroles,password);
					
					//The password supplied is not correct for any role
					if (r == null)
					{
						System.out.println(wrongPassMessage);
						System.exit(1);

					}
					
					Map<String,FilePermission> mfp = r.filePermissions;
					//Get the permissions for that file under the role specified 
					
					// Destroy flag
					if(flag.equals(DESTROYFLAG))
					{	
						String actualFile = filename.replaceFirst("[.][^.]+$", "");
						String cipherFile = folder + filename;
						String inputFile = folder + actualFile;
						String sigFile = inputFile + signatureExtention;
						
						//Get the permissions for that file under the role specified 
						FilePermission fp = mfp.get(actualFile);
						//The filename supplied is not part of the system
						
						if (fp == null)
						{
							System.out.println(noFileMessage);
							System.exit(2);
						}	
						
						if(r.roleName.equals(DESTROYROLENAME) && fp.canRead)
						{
							destroy(fp,cipherFile,password,inputFile,sigFile);
						}
					}
					// Encrypt flag
					else if(flag.equals(ENCRYPTFLAG))
					{
						String inputFile = folder + filename;
						String cipherFile = folder + filename + encryptionExtention;
						String sigFile = folder + filename + signatureExtention;
						
						FilePermission fp = mfp.get(filename);
						//The filename supplied is not part of the system
						
						if (fp == null)
						{
							System.out.println(noFileMessage);
							System.exit(2);
						}	
						
						if(fp.canWrite)
						{
							encrypt(fp, inputFile, password, cipherFile, sigFile);
						}
						else
						{
							System.out.println(noPermission);
						}
					}
					// Decrypt flag
					else if(flag.equals(DECRYPTFLAG))
					{
						String actualFile = filename.replaceFirst("[.][^.]+$", "");
						String cipherFile = folder + filename;
						String inputFile = folder + actualFile;
						String sigFile = inputFile + signatureExtention;
						
						//Get the permissions for that file under the role specified 
						FilePermission fp = mfp.get(actualFile);
						//The filename supplied is not part of the system
						
						if (fp == null)
						{
							System.out.println(noFileMessage);
							System.exit(2);
						}	
						
						if(fp.canRead)
						{
							decrypt(fp,cipherFile,password,inputFile,sigFile);
						}
						else
						{
							System.out.println(noPermission);
						}
					}
				}
				// Check flag
				else if (flag.equals(CHECKFLAG))
				{
					check(mroles);
				}
			}
		}
		catch(Exception e)
		{
			// Do not reveal details of any exception to users
			System.out.println("There was an error in the system. The program is now exiting");
			System.exit(100);

		}
		
			
	}
	
	/**
	 * Encrypt a file using the password provided
	 * @param fp: A container for all the keys needed by a file. [public key, encrypted private key, encrypted AES keys]
	 * @param inputFile: The file to encrypt
	 * @param password: Password supplied by a user
	 * @param cipherFile: The name of the cipher file as output
	 * @param sigFile: The name of the signature file
	 * @throws Exception
	 */
	private static void encrypt(FilePermission fp, String inputFile, String password, String cipherFile, String sigFile) throws Exception
	{
		// No file in filesystem to encrypt
		if (!new File(inputFile).exists())
		{
			System.out.println(noFileEncMsg);
			System.exit(3);
		}
		//File exists to encrypt
		
		// Get the AES key for decrypting the files AES key
		final Key AESKey = Crypto.getKeyFromHash(password);
		
		//Decrypt AES key
		byte[] aeskey = Crypto.decryptKey(fp.getpAESCipherText(), AESKey);
		Key key = Crypto.getKeyFromBytes(aeskey);
		
		//Encrypt the inputfile into the file cipherFile
		Crypto.encryptFile(key, inputFile, cipherFile);
			
		//Sign the Newly created file
		byte[] privateKey = Crypto.decryptKey(fp.getPkeyCipherText(), AESKey);

		PrivateKey priv = Crypto.getPrivateKeyFromBytes(privateKey);
		
//		//Sign the encrypted file so the program does not need to decrypt a file in order to verify its signature. 
		Crypto.createSigniture(priv,cipherFile, sigFile);
		
		//Delete the plaintext version of the file
		File input = new File(inputFile);
		input.delete();
		
		System.out.println(successMessage);
		
	}
	
	/**
	 * Decrypt a file using the password provided
	 * @param fp: A container for all the keys needed by a file. [public key, encrypted private key, encrypted AES keys]
	 * @param cipherFile: File to decrypt
	 * @param password: Password to decrypt the file
	 * @param plaintextFile: Name of the plaintext file
	 * @param sigFile: Name of the signature file for the cipherFile
	 * @throws Exception
	 */
	private static void decrypt(FilePermission fp, String cipherFile, String password, String plaintextFile, String sigFile) throws Exception
	{
		// No cipherfile exists to decrypt
		if (!new File(cipherFile).exists())
		{
			System.out.println(noFileDecMsg);
			System.exit(5);
		}
		
		// No signature file exists to check validity
		if (!new File(sigFile).exists())
		{
			System.out.println(noSigFileMsg);
			System.exit(6);
		}
		
		// Get AES key to decrypt the AES key for the file
		final Key AESKey = Crypto.getKeyFromHash(password);
				
		// Get the public key - which is not encrypted as anyone can check a signature
		PublicKey pub = Crypto.getPublicKeyFromBytes(fp.getPublicKey());
		
		//Signature is not correct
		if(!Crypto.verifySigniture(pub,sigFile, cipherFile))
		{
			System.out.println(wrongSigMsg);
			System.exit(7);
		}
		
		//Signature is correct
		System.out.println(correctSigMsg);
		
		//Decrypt the file after correctly verifying the signature 
		byte[] aeskey = Crypto.decryptKey(fp.getpAESCipherText(), AESKey);
		Key key = Crypto.getKeyFromBytes(aeskey);
		
		Crypto.decryptFile(key, cipherFile, plaintextFile);
		System.out.println(successMessage);
			
	}
	
	/**
	 * Decrypt the file and then remove all permissions and other files.
	 * @param fp: A container for all the keys needed by a file. [public key, encrypted private key, encrypted AES keys]
	 * @param cipherFile: File to decrypt
	 * @param password: Password to decrypt the file
	 * @param plaintextFile: Name of the plaintext file
	 * @param sigFile: Name of the signature file for the cipherFile
	 * @throws Exception
	 */
	private static void destroy(FilePermission fp, String cipherFile, String password, String plaintextFile, String sigFile) throws Exception
	{
		//Decrypt the file
		decrypt(fp,cipherFile,password,plaintextFile,sigFile);
		
		Set<String> knownFiles = Files.getAuxFiles();
		
		//Stop the decrypted file from getting deleted
		knownFiles.add(plaintextFile);
				
		//Delete everything apart from auxillery files
		File dir = new File(folder);
		File[] directoryListing = dir.listFiles();
		if (directoryListing != null) 
		{
			for (File child: directoryListing) 
			{
				if(child.isDirectory())
				{
					//If directory delete without checking what it is
					System.out.println(String.format(unwantedExtraDirectory, child.getName()));
					//Delete any files in the directory
					String[]entries = child.list();
					for(String s: entries)
					{
					    File currentFile = new File(child.getPath(),s);
					    currentFile.delete();
					}
					child.delete();
				}
				else
				{
					// File is not in known files list
					if (!knownFiles.contains(folder + child.getName()))
					{
						//Additional File that is not wanted - delete it
						System.out.println(String.format(unwantedExtraFile,child.getName()));
						child.delete();
					}
				}
			}
		}
		
		//Remove permissions
		Files.removePermissions();
		
	}
	
	
	/**
	 * Check that all files are in order
	 * @throws IOException 
	 * @throws NoSuchAlgorithmException 
	 * @throws SignatureException 
	 * @throws InvalidKeyException 
	 * @throws InvalidKeySpecException 
	 */
	private static void check(Map<String, Role> roles) throws InvalidKeyException, SignatureException, NoSuchAlgorithmException, IOException, InvalidKeySpecException
	{
		//Retain a list of missing files
		ArrayList<String> missingFiles = new ArrayList<String>();
		
		// Get the set of known auxillery files used by the system.
		Set<String> knownFiles = Files.getAuxFiles();
		
		// For each known file in the system
		for (String f: Files.getFiles(roles)) 
		{
			String encryptedFile = folder + f + encryptionExtention;
			String plaintextFile = folder + f;
			String sigFile = folder + f + signatureExtention;
			
			//Get the public key
			PublicKey pub = Crypto.getPublicKeyFromBytes(Files.getPublicKeyForFile(roles,f));
			
			if (!new File(encryptedFile).exists()) 
			{
				// Encrypted File is missing. add it to list of missing files
				missingFiles.add(f + encryptionExtention);
			}
			else 
			{
				if (!new File(sigFile).exists()) 
				{
					// Signature file does not exist
					System.out.println(String.format(sigFileDoesNotExistDelete, encryptedFile, encryptedFile));
					
					// Delete encrypted file
					new File(encryptedFile).delete();
				} 
				else if (!Crypto.verifySigniture(pub, sigFile, encryptedFile)) 
				{
					// Signature for file is not correct
					System.out.println(String.format(sigNotCorrectDelete,
							encryptedFile, encryptedFile, sigFile));
					
					// Delete encrypted and signature files
					new File(encryptedFile).delete();
					new File(sigFile).delete();
				} 
				else 
				{
					// Add to set of files known to be in the folder
					knownFiles.add(encryptedFile); 
					// Note: sig file will be not added to known files and later deleted if the encrypted file did not exist
					knownFiles.add(sigFile); 
				}
			}

			if (new File(plaintextFile).exists()) 
			{
				// Unencrypted file exists - delete it
				System.out.println(String.format(notEncryptedFileExists, f));
				new File(plaintextFile).delete();
			}

		}
		 
		//Check for unwanted additions
		File dir = new File(folder);
		File[] directoryListing = dir.listFiles();
		if (directoryListing != null) 
		{
			for (File child: directoryListing) 
			{
				if(child.isDirectory())
				{
					//If directory delete without checking what it is
					System.out.println(String.format(unwantedExtraDirectory, child.getName()));
					//Delete any files in the directory
					String[]entries = child.list();
					for(String s: entries)
					{
					    File currentFile = new File(child.getPath(),s);
					    currentFile.delete();
					}
					child.delete();
				}
				else
				{
					// File is not in known files list
					if (!knownFiles.contains(folder + child.getName()))
					{
						//Additional File that is not wanted - delete it
						System.out.println(String.format(unwantedExtraFile,child.getName()));
						child.delete();
					}
				}
			}
		}
		
		//Check for missing auxillery files
		for (String file: knownFiles)
		{
			if (!new File(file).exists())
			{
				missingFiles.add(file.split("/")[1]);
			}
		}
		
		//List missing files
		for (String file: missingFiles)
		{
			System.out.println(String.format(missingFile, file));
		}
		 
	}
	

	/**************************Crypto*******************************/	

	/**
	 * Class that contains cryptographic functions
	 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
	 * Student Number: 120121386
	 */
	private static class Crypto 
	{
		 	 
		private static String encryptionAlgorithm = "AES";
		private static String asymetric = "DSA";
		private static String signatureType = "SHA1withDSA";
		private static String cipherAlgorithm = "AES/CBC/PKCS5Padding";
		
		// When encrypting the same file more than one this could be a problem
		private static byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	    private static IvParameterSpec ivspec = new IvParameterSpec(iv);
		
	    
	    /**
	     * Verify that the signature for a file is correct
	     * @param pub: PublicKey 
	     * @param sigFile: Location of the signature file
	     * @param cipherFile : Location of the encrypted file
	     * @return True if signature is correct
	     * @throws IOException
	     * @throws SignatureException
	     * @throws InvalidKeyException
	     * @throws NoSuchAlgorithmException
	     * @throws InvalidKeySpecException 
	     */
	    private static boolean verifySigniture(PublicKey pub, String sigFile,String cipherFile) throws IOException, SignatureException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException
	    {			
	    	Signature s = Signature.getInstance(signatureType); 
			s.initVerify(pub);
			
			File sigF = new File(sigFile);
	        File message = new File(cipherFile);
	        
	        //Get signature bytes
	        FileInputStream sigStream = new FileInputStream(sigF);
	        byte[] sigBytes = new byte[(int) sigF.length()];
	        sigStream.read(sigBytes);
	        
	        //Get message bytes
	        FileInputStream msgStream = new FileInputStream(message);
	        byte[] msgBytes = new byte[(int) message.length()];
	        msgStream.read(msgBytes);
	        
	        msgStream.close();
	        sigStream.close();
	        s.update(msgBytes);
	        //Return verification on bytes
	        try
	        {
				return s.verify(sigBytes);
	        }
	        catch (Exception e)
	        {
	        	return false;
	        }
	    }
	    
	    
	    /**
	     * Create a signature for the given file
	     * @param priv: Private key for signing
	     * @param fileName signature file name to create
	     * @throws NoSuchAlgorithmException 
	     * @throws InvalidKeyException 
	     * @throws IOException 
	     * @throws SignatureException 
	     * @throws NoSuchProviderException 
	     * @param file: File to sign
	     */
	    private static void createSigniture(PrivateKey priv, String file, String fileName) throws NoSuchAlgorithmException, InvalidKeyException, IOException, SignatureException, NoSuchProviderException
	    {
	    	
	    	Signature s = Signature.getInstance(signatureType); 
			s.initSign(priv);
			
	        File input = new File(file);
	        File output = new File(fileName);
	        
	        //Get message bytes for signature generation
	        FileInputStream inputStream = new FileInputStream(input);
	        byte[] inputBytes = new byte[(int) input.length()];
	        inputStream.read(inputBytes);
	        
	        s.update(inputBytes);
	        byte[] sig = s.sign();
	        
	        //Write the signature
	        FileOutputStream outputStream = new FileOutputStream(output);
	        outputStream.write(sig);
	        inputStream.close();
	        outputStream.close();
	        
	    }
	    
	    /**
	     * Get a PublicKey object from its bytes
	     * @param pub: Public key 
	     * @return Public Key
	     * @throws InvalidKeySpecException
	     * @throws NoSuchAlgorithmException
	     */
	    private static PublicKey getPublicKeyFromBytes(byte[] pub) throws InvalidKeySpecException, NoSuchAlgorithmException
	    {
	    	X509EncodedKeySpec ks = new X509EncodedKeySpec(pub);
			KeyFactory kf = KeyFactory.getInstance(asymetric);
			return kf.generatePublic(ks);
	    }
	    
	    /**
	     * Return a PrivateKey object from its bytes
	     * @param priv: Private key bytes
	     * @return PrivateKey
	     * @throws InvalidKeySpecException
	     * @throws NoSuchAlgorithmException
	     */
	    private static PrivateKey getPrivateKeyFromBytes(byte[] priv) throws InvalidKeySpecException, NoSuchAlgorithmException
	    {
	    	PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(priv);
			KeyFactory kf = KeyFactory.getInstance(asymetric);
			return kf.generatePrivate(ks);
	    }
	    
	    
	    /**
	     * Generate KeyPairs
	     * @return KeyPair
	     * @throws NoSuchAlgorithmException
	     * @throws NoSuchProviderException
	     */
	    private static KeyPair generateKeyPairs() throws NoSuchAlgorithmException, NoSuchProviderException
	    {
	    	KeyPairGenerator keyGen = KeyPairGenerator.getInstance(asymetric);
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
			keyGen.initialize(1024, random);
			return keyGen.generateKeyPair();
	    }
	    
	    
	    /**
	     * Encrypt a file
	     * @param key : Key for file encryption
	     * @param inputFile : File to encrypt
	     * @param outputFile : Cipher file 
	     * @throws Exception
	     */
	    private static void encryptFile(Key key, String inputFile, String outputFile) throws Exception
	    {
	        Cipher cipher = Cipher.getInstance(cipherAlgorithm);
	        cipher.init(Cipher.ENCRYPT_MODE, key,ivspec);
	        fileCipher(inputFile,outputFile,cipher);
	    }
	    
	    /**
	     * Perform cipher operation on file
	     * @param inputFile : Input for cipher operation
	     * @param outputFile : Output for cipher operation
	     * @param cipher : Cipher to use
	     * @throws Exception
	     */
	    private static void fileCipher(String inputFile, String outputFile,Cipher cipher) throws Exception
	    {
	        File input = new File(inputFile);
	        File output = new File(outputFile);
	        //Read in input bytes
	        FileInputStream inputStream = new FileInputStream(input);
	        byte[] inputBytes = new byte[(int) input.length()];
	        inputStream.read(inputBytes);
	        //Encrypt input bytes
	        byte[] outputBytes = cipher.doFinal(inputBytes);
	        // Write encrypted bytes
	        FileOutputStream outputStream = new FileOutputStream(output);
	        outputStream.write(outputBytes);
	        inputStream.close();
	        outputStream.close();
	    }
	    
	    /**
	     * Decrypt a file
	     * @param key : Key for decryption
	     * @param inputFile : File to decrypt
	     * @param outputFile : Plaintext file
	     * @throws Exception
	     */
	    private static void decryptFile(Key key, String inputFile, String outputFile) throws Exception
	    {
	        Cipher cipher = Cipher.getInstance(cipherAlgorithm);
	        cipher.init(Cipher.DECRYPT_MODE, key,ivspec);
	        fileCipher(inputFile,outputFile,cipher);
	    }
	    
		/**
		 * Get the Key version of a key in byte form
		 * @param key : key in bytes
		 * @return Key object
		 * @throws Exception
		 */
		private static Key getKeyFromBytes(byte[] key) throws Exception
		{
			return generateKeyFromBytes(key,encryptionAlgorithm);
		}
		
		/**
		 * Check a password against a given hash and salt
		 * @param hash : Hash bytes
		 * @param salt : salt bytes
		 * @param password : plaintext password
		 * @return
		 */
		private static boolean checkPassword(byte[] hash, byte[] salt, String password)
		{
			return Arrays.equals(hash, Crypto.hashPasswordSHA256(password, salt));
		}
		
		
		/**
		 * Generate a key for the specified encryption algorithm
		 * @return : SecretKey object
		 * @throws NoSuchAlgorithmException
		 */
		private static SecretKey generateKey() throws NoSuchAlgorithmException
		{
		    KeyGenerator keyGen = KeyGenerator.getInstance(encryptionAlgorithm);
		    return keyGen.generateKey();
		}
		
		/**
		 * Get a key from hashing the supplied plaintext
		 * @param plaintext : Plaintext password
		 * @return Key for encrypting/decrypting other keys
		 * @throws Exception
		 */
		private static Key getKeyFromHash(String plaintext) throws Exception
		{
			return generateKeyFromBytes(Crypto.hashPasswordMD5(plaintext),encryptionAlgorithm);
		}
		
		/**
		 * Encrypt a AES key 
		 * @param aeskey: aesKey for encryption
		 * @param key: Key to encrypt aes key
		 * @return encrypted aes key as hex string
		 * @throws Exception
		 */
		private static String encryptKey(SecretKey aeskey, Key key) throws Exception
		{
			Cipher aes = Cipher.getInstance(cipherAlgorithm);
			// Encrypt the files aes key using a read AES key
			aes.init(Cipher.ENCRYPT_MODE, key,ivspec);
			return getHexStr(aes.doFinal(aeskey.getEncoded())); 
		}
		
		/**
		 * Encrypt a private key
		 * @param privkey: privkey for encryption
		 * @param key: key to encrypt privkey
		 * @return encrypted private key as hex string
		 * @throws Exception
		 */
		private static String encryptKey(PrivateKey privkey, Key key) throws Exception
		{
			Cipher aes = Cipher.getInstance(cipherAlgorithm);
			// Encrypt the files aes key using a read AES key
			aes.init(Cipher.ENCRYPT_MODE, key,ivspec);
			return getHexStr(aes.doFinal(privkey.getEncoded())); 
		}
		
		
		/**
		 * Decrypt the AES key
		 * @param cipherText: Encrypted key
		 * @param key: Key for decryption
		 * @return Decrypted key in byte form
		 * @throws Exception
		 */
		private static byte[] decryptKey(String cipherText, Key key) throws Exception
		{
			Cipher aes = Cipher.getInstance(cipherAlgorithm);
			aes.init(Cipher.DECRYPT_MODE, key,ivspec);
			return aes.doFinal(hexStr2BArr(cipherText));
		}
		
		/**
		 * Generate a Key object from a byte key
		 * @param secKey : key in byte form
		 * @param algorithm : algorithm for key 
		 * @return Key object
		 * @throws Exception
		 */
		private static Key generateKeyFromBytes(final byte[] secKey, String algorithm) throws Exception 
		{
		    final Key key = new SecretKeySpec(secKey, algorithm);
		    return key;
		}
		
		
		/**
		 * Hash plaintext using SHA 256 and return hash in hexadecimal format
		 * @param plaintext : Plaintext password
		 * @param salt : Random salt in byte form
		 * @return Hash in bytes
		 */
	    private static byte[] hashPasswordSHA256(String plaintext, byte[] salt)
	    {
	    	byte[] bytes = null;
	        try 
	        {
	            MessageDigest md = MessageDigest.getInstance("SHA-256");
	            md.update(salt);
	            bytes = md.digest(plaintext.getBytes());            
	        } 
	        catch (NoSuchAlgorithmException e) 
	        {
	            System.out.println("Hash password error");
	        }
	        return bytes;
	    }
	    
		/**
		 * Hash plaintext using MD5 (128 bit) and return hash in hexadecimal format
		 * @param plaintext : Plaintext password
		 * @return hash in byte form.
		 */
	    private static byte[] hashPasswordMD5(String plaintext)
	    {
	    	byte[] bytes = null;
	        try 
	        {
	            MessageDigest md = MessageDigest.getInstance("MD5");
	            bytes = md.digest(plaintext.getBytes()); 
	        } 
	        catch (NoSuchAlgorithmException e) 
	        {
	            System.out.println("Hash password error");
	        }
	        return bytes;
	    }
	    
	     
	    /**
	     * Generate Random Salt for hashing algorithm
	     * @return Random salt in byte form
	     * @throws NoSuchAlgorithmException
	     */
	    private static byte[] getSalt() throws NoSuchAlgorithmException
	    {
	    	//Use SecureRandom for pseudo random number generator algorithm
	        SecureRandom sr = new SecureRandom();
	        byte[] salt = new byte[16];
	        sr.nextBytes(salt);
	        return salt;
	    }
	    
	    /**
	     * Get Hexadecimal string version of array of bytes
	     * @param bytes: Bytes for transformation to hex string
	     * @return Random salt in byte form
	     */
	    private static String getHexStr(byte[] bytes)
	    {
	    	StringBuilder sb = new StringBuilder();
	        // Create HEX string
	        for (byte b : bytes) 
	        {
	            sb.append(String.format("%02x", b));
	        }
	        return sb.toString();
	    	
	    }
	    
	    /**
	     * Convert a HexString into an array of bytes
	     * 
	     * Taken from: http://stackoverflow.com/questions/140131/convert-a-string-representation-of-a-hex-dump-to-a-byte-array-using-java
	     * 
	     */
	    private static byte[] hexStr2BArr(String s) 
	    {
	        int len = s.length();
	        byte[] data = new byte[len / 2];
	        for (int i = 0; i < len; i += 2) 
	        {
	            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                                 + Character.digit(s.charAt(i+1), 16));
	        }
	        return data;
	    }
	    
			
	}
	
	/**************************Files*******************************/	
	/**
	 * 
	 * SETUP CLASS and 
	 * 
	 * Class for reading/writing from/to files that contain hashed passwords, salts and encrypted keys
	 * 
	 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
 	 * Student Number: 120121386
	 */
	private static class Files 
	{
	
	    private static ArrayList<RoleRecord> roles = new ArrayList<RoleRecord>();
	    private static List<String> knownfiles = new ArrayList<String>();
	            
		private static String pwfile =  folder + "passwords_list";
		private static String permissionsFile =  folder + "ACM";

		private static String hpwFile = folder + "passwords_list_hashed";
		private static String saltFile = folder + "passwords_list_salts";
		private static String encryptedAESFile = folder + "encrypted_aes_keys";
		private static String keyPairFile = folder + "keypairs";

		
		private static final String WRITEPERMISSION = "write";
		private static final String READPERMISSION = "read";
			
		/**
		 * Run setup - Not used in Protect normal operation
		 * @throws Exception
		 */
		private static void setup() throws Exception 
		{
			//Read in plaintext for config startup
			Files.readRoles(pwfile);
			// Make encrypted passwords and keys
			Files.getKnownFiles(permissionsFile);
			Files.makeFiles(hpwFile,saltFile,encryptedAESFile,keyPairFile);
			
		}
		
		/**
		 * Read in a specified plaintext password file for roles
		 * @param pwfile : Password file
		 */
		private static void readRoles(String pwfile)
		{
		    Scanner sc;
			try 
			{
				sc = new Scanner(new FileReader(pwfile));
			    while (sc.hasNextLine())
			    {
			    	String[] split= sc.nextLine().replace("\n", "").replace("\r","").split("\t");
			    	roles.add(new RoleRecord(split[0],split[1]));
			    }
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			}
			
		}
		
		/**
		 * Get all known files 
		 * @param permissionsFile: Role permissions file
		 */
		private static void getKnownFiles(String permissionsFile)
		{
		    Scanner sc;
			try 
			{
				sc = new Scanner(new FileReader(permissionsFile));
				List<String> split = new ArrayList<String>(Arrays.asList(sc.nextLine().replace("\n", "").replace("\r","").split("\t")));
				knownfiles = split;
				knownfiles.remove(0); //Remove missing file entry caused by matrix format
			} 
			catch (FileNotFoundException e) 
			{
				System.out.println("Read files error");
			}
			
		}
		
		
		/**
		 * Get files from role map 
		 * @param roles: All roles
		 */
		private static Set<String> getFiles(Map<String,Role> roles)
		{
		    //get any role
		    Role r = roles.values().iterator().next();
		    //Return all files in the system
		    return r.filePermissions.keySet();

		}
		
		/**
		 * Get public key for a file 
		 * @param roles: All roles
		 * @param file: file for querying
		 * @return public key in bytes
		 */
		private static byte[] getPublicKeyForFile(Map<String,Role> roles, String file)
		{
		    //get any role
		    Role r = roles.values().iterator().next();
		    //Return all files in the system
		    return r.filePermissions.get(file).publicKey;

		}
		
		
		/**
		 * Make all the hashed passwords, salt and encrypted AES files from stored plaintext passwords
		 * @param hFileName : file for hashed passwords
		 * @param sFileName : file for salts
		 * @param aesFileName: File for encrypted aes keys
		 * @param keyPairFile: File for key pairs
		 * @throws Exception
		 */
		private static void makeFiles(String hFileName,String sFileName,String aesFileName,String keyPairFile) throws Exception
		{
			PrintWriter hwriter; //hashed password file
			PrintWriter swriter; //salts password file
			PrintWriter aeswriter; //Encrypted AES keys file
			PrintWriter kpwriter; //Public and private keys
			try 
			{
				hwriter = new PrintWriter(hFileName, "UTF-8");
				swriter = new PrintWriter(sFileName,"UTF-8");
				aeswriter = new PrintWriter(aesFileName,"UTF-8");
				kpwriter = new PrintWriter(keyPairFile,"UTF-8");
				
				for(int j = 0; j < roles.size(); j++)
				{
					RoleRecord f = roles.get(j);	
					//Read in the plaintext password for each role
					String p = f.getItem();
					//Generate random salt for each password
					byte[] psalt = Crypto.getSalt();
					//Generate random hash for each password
					byte[] phash = Crypto.hashPasswordSHA256(p, psalt);
				
					hwriter.println(String.format("%s\t%s", f.roleName, Crypto.getHexStr(phash)));
					swriter.println(String.format("%s\t%s",f.roleName, Crypto.getHexStr(psalt)));
					
				}
				
				// Write header
				String listString = "";
				for (String s : knownfiles)
				{
				    listString += "\t" + s;
				}		
				aeswriter.println(listString);
				kpwriter.println(listString);

				//Generate AES, private, public keys
				SecretKey[] aeskeys = new SecretKey[knownfiles.size()];
				PrivateKey[] privkeys = new PrivateKey[knownfiles.size()];
				PublicKey[] publickeys = new PublicKey[knownfiles.size()];

				for(int i = 0; i< knownfiles.size(); i++)
				{
					aeskeys[i] = Crypto.generateKey();
					// DSA Key pair generator
					KeyPair pair = Crypto.generateKeyPairs();
					privkeys[i] = pair.getPrivate();
					publickeys[i] = pair.getPublic();
					
				}	
				
				
				// encrypt file AES keys them
				// Generate public/private keys
				for(int i = 0; i < roles.size(); i++)
				{
					RoleRecord f = roles.get(i);

					//Read in the plaintext password for each role
					String p = f.getItem();
					//Generate a 128 bit key from each password hash to encrypts the files AES key
					final Key pAESKey = Crypto.getKeyFromHash(p);

					aeswriter.print(String.format("%s", f.roleName));
					kpwriter.print(String.format("%s", f.roleName));

					for(int j = 0; j < knownfiles.size(); j++)
					{						
						//Get the files AESKey
						SecretKey aeskey = aeskeys[j];
						//Get priv key 
						PrivateKey privkey = privkeys[j];
						//Get public key
						PublicKey publickey = publickeys[j];
						
						// Encrypt the files AES key
						String pAESCipherText = Crypto.encryptKey(aeskey, pAESKey);
										
						// Encrypt the private key using a hashed version of the write password
						// Public Key does not need to be encrypted
						String privKeyCipherText = Crypto.encryptKey(privkey, pAESKey);
												
						// Write all ciphertext items to files.
						aeswriter.print(String.format("\t%s",pAESCipherText));
						kpwriter.print(String.format("\t%s,%s", Crypto.getHexStr(publickey.getEncoded()),privKeyCipherText));
						
					}
					aeswriter.println();	
					kpwriter.println();
				}
				
				hwriter.close();
				swriter.close();
				aeswriter.close();
				kpwriter.close();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		
		/**
		 * Remove all permissions from the permission file: used when destroy flag is opted
		 * @throws FileNotFoundException
		 * @throws UnsupportedEncodingException
		 */
		private static void removePermissions() throws FileNotFoundException, UnsupportedEncodingException
		{
			String temp = permissionsFile + "temp";
			PrintWriter tempPermissions = new PrintWriter(temp,"UTF-8");
			Scanner sc;
			try 
			{
				sc = new Scanner(new FileReader(permissionsFile));
				//Print header
				tempPermissions.println(sc.nextLine());
				
			    while (sc.hasNextLine())
			    {
			    	String[] split= sc.nextLine().replace("\n", "").replace("\r","").split("\t");
			    	tempPermissions.print(split[0]);
			    	for(int i = 1; i<split.length; i++)
			    	{
			    		tempPermissions.print("\t" + ",,");
			    	}
			    	
			    	tempPermissions.println();
			    }
			    
				sc.close();
			} 
			catch (FileNotFoundException e) 
			{
				System.out.println("Read error");
			}
			
			tempPermissions.close();
			
			File old = new File(temp);
			File newp = new File(permissionsFile);
			//Remove old permissions file
			old.renameTo(newp);
			
		}
		
	
		/**
		 * Return set of auxiliary files used by the program.
		 * @return set of files
		 */
		private static Set<String> getAuxFiles()
		{
			Set<String> s = new HashSet<String>();
			s.add(hpwFile);
			s.add(saltFile);
			s.add(encryptedAESFile);
			s.add(permissionsFile);
			s.add(keyPairFile);
			
			s.add("./Protect.java");
			s.add("./Protect.class");
			s.add("./Protect$Crypto.class");
			s.add("./Protect$Files.class");
			s.add("./Protect$1.class");
			s.add("./Protect$FilePermission.class");
			s.add("./Protect$Role.class");
			s.add("./Protect$RoleRecord.class");
			return s;
		}
		
		
		/**
		 * Check the password against all roles
		 * @return null if password is false or FilePermissions map for the associated role if true
		 */
		private static Role checkPassword(Map<String,Role> mroles, String plaintextpw)
		{
			
			for (Role r : mroles.values()) 
			{
				if (Crypto.checkPassword(r.getHashedPassword(), r.getSalt(), plaintextpw))
				{
					return r;
				}
			}
			
			return null;
		}
		
		
		/**
		 * Get all roles, their permissions and hashed passwords
		 * @return Roles
		 */
		private static Map<String,Role> getRoles()
		{
		    Scanner sc;
			Map<String,Role> mroles = new HashMap<String,Role>();

			try 
			{
				//Get all file permissions for a role
				sc = new Scanner(new FileReader(permissionsFile));
				List<String> files = new ArrayList<String>(Arrays.asList(sc.nextLine().replace("\n", "").replace("\r","").split("\t")));
			    while (sc.hasNextLine())
			    {
			    	String[] permissions = sc.nextLine().replace("\n", "").replace("\r","").split("\t");
			    	//Set up new role
	    			Role r = new Role(permissions[0]);
	    			Map<String,FilePermission> fps = new HashMap<String,FilePermission>();
			    	for (int i = 1; i < permissions.length; i++)
			    	{
			    		String f = files.get(i);
			    		Set<String> rwp = new HashSet<String>(Arrays.asList(permissions[i].split(",")));
			    		fps.put(f,new FilePermission(f, rwp.contains(READPERMISSION), rwp.contains(WRITEPERMISSION)));	
			    	}
			    	r.setFilePermissions(fps);
			    	mroles.put(permissions[0], r);

			    }
			    
				//Get encrypted AES keys for each role/file
				sc = new Scanner(new FileReader(encryptedAESFile));
				files = new ArrayList<String>(Arrays.asList(sc.nextLine().replace("\n", "").replace("\r","").split("\t")));
			    while (sc.hasNextLine())
			    {
			    	String[] aesencryptions = sc.nextLine().replace("\n", "").replace("\r","").split("\t");
			    	//Set up new role
			    	Role r = mroles.get(aesencryptions[0]);
			    	Map<String,FilePermission> fps = r.getFilePermissions();
			    	
			    	for (int i = 1; i < aesencryptions.length; i++)
			    	{
			    		String f = files.get(i);
			    		FilePermission fp = fps.get(f);
			    		fp.setpAESCipherText((aesencryptions[i]));
			    		fps.put(f,fp);	
			    	}
			    	
			    	r.setFilePermissions(fps);
			    	mroles.put(aesencryptions[0], r);

			    }
			    
			    // Read in private and public keys
			    sc = new Scanner(new FileReader(keyPairFile));
				files = new ArrayList<String>(Arrays.asList(sc.nextLine().replace("\n", "").replace("\r","").split("\t")));
			    while(sc.hasNextLine())
			    {
			    	String[] split= sc.nextLine().replace("\n", "").replace("\r","").split("\t");
			    	Role r = mroles.get(split[0]);
			    	Map<String,FilePermission> fps = r.getFilePermissions();
			    	for (int i = 1; i < split.length; i++)
			    	{
			    		String f = files.get(i);
			    		FilePermission fp = fps.get(f);
			    		String[] keypairs = split[i].split(",");
				    	fp.setPublicKey(Crypto.hexStr2BArr(keypairs[0]));
				    	fp.setPkeyCipherText(keypairs[1]);
			    		fps.put(f,fp);	

			    	}
			    	
			    	r.setFilePermissions(fps);
			    	mroles.put(split[0], r);
			    
				}
			    
			    // Get hashed passwords for roles
				sc = new Scanner(new FileReader(hpwFile));
			    while (sc.hasNextLine())
			    {
			    	String[] split = sc.nextLine().replace("\n", "").replace("\r","").split("\t");
			    	Role r = mroles.get(split[0]);
			    	r.setHashedPassword(Crypto.hexStr2BArr(split[1]));
			    	mroles.put(split[0], r);
			    }
			    
			    // Get corresponding salts for roles
				sc = new Scanner(new FileReader(saltFile));
			    while (sc.hasNextLine())
			    {
			    	String[] split = sc.nextLine().replace("\n", "").replace("\r","").split("\t");
			    	Role r = mroles.get(split[0]);
			    	r.setSalt(Crypto.hexStr2BArr(split[1]));
			    	mroles.put(split[0], r);
			    }
			    
			} 
			catch (FileNotFoundException e) 
			{
				System.out.println("File read error");
			}

			return mroles;
		}

	}
	
	
	/************************ Role ******************************/
	/**
	 * To store a role and its associated file permissions
	 *
	 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
	 * Student Number: 120121386
	 */
	private static class Role 
	{
		private String roleName;
		private Map<String,FilePermission> filePermissions = new HashMap<String,FilePermission>();
		private byte[] hashedPassword;
		private byte[] salt;
		
		private Role(String roleName)
		{
			this.roleName = roleName;
		}
		
		@Override
		public String toString() 
		{
			// do not display password
			return "roleName=" + this.roleName + ", Permissions=" + this.filePermissions;
		}
		
		private String getRoleName() {
			return roleName;
		}
		private void setRoleName(String roleName) {
			this.roleName = roleName;
		}
		
		private Map<String, FilePermission> getFilePermissions() {
			return filePermissions;
		}

		private void setFilePermissions(Map<String, FilePermission> filePermissions) {
			this.filePermissions = filePermissions;
		}

		private byte[] getHashedPassword() {
			return hashedPassword;
		}

		private void setHashedPassword(byte[] hashedPassword) {
			this.hashedPassword = hashedPassword;
		}

		private byte[] getSalt() {
			return salt;
		}

		private void setSalt(byte[] salt) {
			this.salt = salt;
		}
		
	
	}	
	
	/************************ File Permission ******************************/
	/**
	 * To store a file and its associated read/write permissions
	 *
	 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
	 * Student Number: 120121386
	 */
	private static class FilePermission
	{

		private String fileName;
		private boolean canRead;
		private boolean canWrite;
		private String pAESCipherText; //AES key encrypted using hashed password
		private String pkeyCipherText; //Encrypted private key using hashed password
		private byte[] publicKey;  // Unencrypted public key
		
		private FilePermission(String fileName, boolean canRead, boolean canWrite)
		{
			this.fileName = fileName;
			this.canRead = canRead;
			this.canWrite = canWrite;
		}
		
		@Override
		public String toString() 
		{
			return "fileName=" + this.fileName + ", canRead? " + this.canRead + ", canWrite? " + this.canWrite;
		}
		
		private String getFileName() {
			return fileName;
		}

		private void setFileName(String fileName) {
			this.fileName = fileName;
		}

		private boolean isCanRead() {
			return canRead;
		}

		private void setCanRead(boolean canRead) {
			this.canRead = canRead;
		}

		private boolean isCanWrite() {
			return canWrite;
		}

		private void setCanWrite(boolean canWrite) {
			this.canWrite = canWrite;
		}
		
		private String getpAESCipherText() 
		{
			return pAESCipherText;
		}
		
		private void setpAESCipherText(String rpAESCipherText) 
		{
			this.pAESCipherText = rpAESCipherText;
		}
		
		private String getPkeyCipherText() 
		{
			return pkeyCipherText;
		}

		private void setPkeyCipherText(String pkeyCipherText) 
		{
			this.pkeyCipherText = pkeyCipherText;
		}
		
		private byte[] getPublicKey() 
		{
			return publicKey;
		}

		private void setPublicKey(byte[] publicKey) 
		{
			this.publicKey = publicKey;
		}
		
	}
	
	
	/************************ Role Record ******************************/
	/**
	 * To store a role and plaintext password - only used for running configuration setup
	 *
	 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
	 * Student Number: 120121386
	 */
	private static class RoleRecord 
	{
	
		private String roleName;
		private String item;
		
		private RoleRecord(String fileName,String item)
		{
			this.roleName = fileName;
			this.item = item;
		}
		
		@Override
		public String toString() 
		{
			return "fileName=" + roleName + ", item=" + item;
		}

		private String getRoleName() {
			return roleName;
		}

		private void setRoleName(String roleName) {
			this.roleName = roleName;
		}

		private String getItem() {
			return item;
		}

		private void setItem(String item) {
			this.item = item;
		}
	
			
	}
	
}
