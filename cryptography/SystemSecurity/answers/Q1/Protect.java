import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Protect class for Question 1
 * 
 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
 * Student Number: 120121386
 */
public class Protect {
	
	/*************** Messages **************/
	private static String successMessage = "Succesfully Completed!";
	private static String correctPassMessage = "Correct Password!";
	private static String wrongPassMessage = "Wrong Password!";
	private static String noFileMessage = "File name supplied does not exist in our records";
	private static String correctSigMsg = "Correct Signiture!";
	private static String wrongSigMsg = "Wrong Signiture!";
	
	private static String noFileEncMsg = "There is not a file in the file system to encrypt";
	private static String noFileDecMsg = "There is not a file in the file system to decrypt";
	private static String noSigFileMsg = "There is not a signature file for the specified file to decrypt";
	
	private static String notEncryptedFileExists = "An unecrypted version of %s exists. Deleting it";
	private static String missingFile = "%s is missing!";
	private static String unwantedExtraFile = "%s is an unwanted extra file. Deleting it";
	private static String unwantedExtraDirectory = "Deleting additional directory %s";
	private static String sigNotCorrectDelete = "Signature for %s is not correct. Removing %s and %s ";
	private static String sigFileDoesNotExistDelete = "Signature for %s does not exist. Removing %s";
	
	/*************** Settings **************/

	private static String encryptionExtention = ".enc";
	private static String signatureExtention = ".sig";
	private static String folder = "./";
	
	private static final String ENCRYPTFLAG = "-e";
	private static final String DECRYPTFLAG = "-d";
	private static final String CHECKFLAG = "-c";

	public static void main(String[] args)
	{
		int length = args.length;
		
		try
		{
			if(length > 0)
			{
				String flag = args[0];
				//Read in information (encrypted keys/passwords) for each file
				Map<String,FileKey> filekeys = Files.getFileKeys();

				if(length == 3)
				{
					String filename = args[1];
					String password = args[2];
				
					// Encrypt flag
					if(flag.equals(ENCRYPTFLAG))
					{
						String inputFile = folder + filename;
						String cipherFile = folder + filename + encryptionExtention;
						String sigFile = folder + filename + signatureExtention;
			
						FileKey fk = filekeys.get(filename);
						//The filename supplied is not part of the system
						if (fk == null)
						{
							System.out.println(noFileMessage);
							System.exit(1);
						}	
						encrypt(fk, inputFile, password, cipherFile, sigFile);
					}
					// Decrypt flag
					else if(flag.equals(DECRYPTFLAG))
					{
						String actualFile = filename.replaceFirst("[.][^.]+$", "");
						String cipherFile = folder + filename;
						String inputFile = folder + actualFile;
						String sigFile = inputFile + signatureExtention;
			
						FileKey fk = filekeys.get(actualFile);
						//The filename supplied is not part of the system
						if (fk == null)
						{
							System.out.println(noFileMessage);
							System.exit(1);
						}	
						
						decrypt(fk,cipherFile,password,inputFile,sigFile);
					}
				}
				// Check flag
				else if (flag.equals(CHECKFLAG))
				{
					check(filekeys);
				}
			}
		}
		catch(Exception e)
		{
			// Do not reveal details of any exception to users
			System.out.println("There was an error in the system. The program is now exiting");
			System.exit(100);
		}

			
	}
	
	/**
	 * Encrypt a file using the password provided
	 * @param fk: A container for all the keys needed by a file. [public key, encrypted private key, encrypted AES keys]
	 * @param inputFile: The file to encrypt
	 * @param password: Password supplied by a user
	 * @param cipherFile: The name of the cipher file as output
	 * @param sigFile: The name of the signature file
	 * @throws Exception
	 */
	private static void encrypt(FileKey fk, String inputFile, String password, String cipherFile, String sigFile) throws Exception
	{
		// Not correct password
		if(!Crypto.checkPassword(fk.getWpHash(), fk.getWpSalt(), password))
		{
			System.out.println(wrongPassMessage);
			System.exit(2);
		}
		
		// Correct Write password for file
		System.out.println(correctPassMessage);
		
		// No file in filesystem to encrypt
		if (!new File(inputFile).exists())
		{
			System.out.println(noFileEncMsg);
			System.exit(3);
		}
		//File exists to encrypt
		
		// Get the AES key for decrypting the files AES key
		final Key wpAESKey = Crypto.getKeyFromHash(password);
		//Decrypt AES key
		byte[] aeskey = Crypto.decryptKey(fk.getWpAESCipherText(), wpAESKey);
		Key key = Crypto.getKeyFromBytes(aeskey);
		
		//Encrypt the inputfile into the file cipherFile
		Crypto.encryptFile(key, inputFile, cipherFile);
		
		//Sign the Newly created file
		byte[] privateKey = Crypto.decryptKey(fk.getPkeyCipherText(), wpAESKey);

		PrivateKey priv = Crypto.getPrivateKeyFromBytes(privateKey);
		
		//Sign the encrypted file so the program does not need to decrypt a file in order to verify its signature. 
		Crypto.createSigniture(priv, cipherFile, sigFile);
		
		//Delete the plaintext version of the file
		File input = new File(inputFile);
		input.delete();
		
		System.out.println(successMessage);
		
	}
	
	/**
	 * Decrypt a file using the password provided
	 * @param fk: A container for all the keys needed by a file. [public key, encrypted private key, encrypted AES keys]
	 * @param cipherFile: File to decrypt
	 * @param password: Password to decrypt the file
	 * @param plaintextFile: Name of the plaintext file
	 * @param sigFile: Name of the signature file for the cipherFile
	 * @throws Exception
	 */
	private static void decrypt(FileKey fk, String cipherFile, String password, String plaintextFile, String sigFile) throws Exception
	{
		// Wrong password provided
		if(!Crypto.checkPassword(fk.getRpHash(), fk.getRpSalt(), password))
		{
			System.out.println(wrongPassMessage);
			System.exit(4);
		}
		
		// Correct Write password for file
		System.out.println(correctPassMessage);
		
		// No cipherfile exists to decrypt
		if (!new File(cipherFile).exists())
		{
			System.out.println(noFileDecMsg);
			System.exit(5);
		}
		
		// No signature file exists to check validity
		if (!new File(sigFile).exists())
		{
			System.out.println(noSigFileMsg);
			System.exit(6);
		}
		
		// Get AES key to decrypt the AES key for the file
		final Key rpAESKey = Crypto.getKeyFromHash(password);
		
		// Get the public key - which is not encrypted as anyone can check a signature
		PublicKey pub = Crypto.getPublicKeyFromBytes(fk.getPublicKey());
		
		//Signature is not correct
		if(!Crypto.verifySigniture(pub, sigFile, cipherFile))
		{
			System.out.println(wrongSigMsg);
			System.exit(7);
		}
		
		//Signature is correct
		System.out.println(correctSigMsg);
		
		//Decrypt the file after correctly verifying the signature 
		byte[] aeskey = Crypto.decryptKey(fk.getRpAESCipherText(), rpAESKey);
		Key key = Crypto.getKeyFromBytes(aeskey);
		
		Crypto.decryptFile(key, cipherFile, plaintextFile);
		System.out.println(successMessage);
			
	}
	
	/**
	 * Check that all files are in order
	 * @throws IOException 
	 * @throws NoSuchAlgorithmException 
	 * @throws SignatureException 
	 * @throws InvalidKeyException 
	 * @throws InvalidKeySpecException 
	 */
	private static void check(Map<String, FileKey> filekeys) throws InvalidKeyException, SignatureException, NoSuchAlgorithmException, IOException, InvalidKeySpecException
	{
		//Retain a list of missing files
		ArrayList<String> missingFiles = new ArrayList<String>();
		
		// Get the set of known auxillery files used by the system - password/salt ect.
		Set<String> knownFiles = Files.getAuxFiles();
		
		// For each known file in the system
		for (FileKey fk: filekeys.values()) 
		{
			String encryptedFile = folder + fk.fileName + encryptionExtention;
			String plaintextFile = folder + fk.fileName;
			String sigFile = folder + fk.fileName + signatureExtention;
			
			//Get the public key
			PublicKey pub = Crypto.getPublicKeyFromBytes(fk.getPublicKey());
			
			if (!new File(encryptedFile).exists()) 
			{
				// Encrypted File is missing. add it to list of missing files
				missingFiles.add(fk.fileName + encryptionExtention);
			} 
			else 
			{
				if (!new File(sigFile).exists()) 
				{
					// Signature file does not exist
					System.out.println(String.format(sigFileDoesNotExistDelete, encryptedFile, encryptedFile));
					
					// Delete encrypted file
					new File(encryptedFile).delete();
				} 
				else if (!Crypto.verifySigniture(pub, sigFile, encryptedFile)) 
				{
					// Signature for file is not correct
					System.out.println(String.format(sigNotCorrectDelete,
							encryptedFile, encryptedFile, sigFile));
					
					// Delete encrypted and signature files
					new File(encryptedFile).delete();
					new File(sigFile).delete();
				} 
				else 
				{
					// Add to set of files known to be in the folder
					knownFiles.add(encryptedFile); 
					
					// Note: sig file will be not added to known files and later deleted if the encrypted file did not exist
					knownFiles.add(sigFile); 
				}
			}

			if (new File(plaintextFile).exists()) 
			{
				// Unencrypted file exists - delete it
				System.out.println(String.format(notEncryptedFileExists, fk.fileName));
				new File(plaintextFile).delete();
			}

		}
		 
		//Check for unwanted additions
		File dir = new File(folder);
		File[] directoryListing = dir.listFiles();
		if (directoryListing != null) 
		{
			for (File child: directoryListing) 
			{
				if(child.isDirectory())
				{
					//If directory delete without checking what it is
					System.out.println(String.format(unwantedExtraDirectory, child.getName()));
					//Delete any files in the directory
					String[]entries = child.list();
					for(String s: entries)
					{
					    File currentFile = new File(child.getPath(),s);
					    currentFile.delete();
					}
					child.delete();
				}
				else
				{
					// File is not in known files list
					if (!knownFiles.contains(folder + child.getName()))
					{
						//Additional File that is not wanted - delete it
						System.out.println(String.format(unwantedExtraFile,child.getName()));
						child.delete();
					}
				}
			}
		}
		
		//Check that no auxillery files are missing
		for (String file: knownFiles)
		{
			if (!new File(file).exists())
			{
				//Add just the name of the file
				missingFiles.add(file.split("/")[1]);
			}
		}
		
		//List missing files
		for (String file: missingFiles)
		{
			System.out.println(String.format(missingFile, file));
		}
		 
	}
	

	

	/**************************Crypto*******************************/	

	/**
	 * Class that contains cryptographic functions
	 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
	 * Student Number: 120121386
	 */
	private static class Crypto 
	{
		 	 
		private static String encryptionAlgorithm = "AES";
		private static String asymetric = "DSA";
		private static String signatureType = "SHA1withDSA";
		private static String cipherAlgorithm = "AES/CBC/PKCS5Padding";
		private static byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	    private static IvParameterSpec ivspec = new IvParameterSpec(iv);
		
	    
	    /**
	     * Verify that the signature for a file is correct
	     * @param pub: PublicKey 
	     * @param sigFile: Location of the signature file
	     * @param cipherFile : Location of the encrypted file
	     * @return True if signature is correct
	     * @throws IOException
	     * @throws SignatureException
	     * @throws InvalidKeyException
	     * @throws NoSuchAlgorithmException
	     * @throws InvalidKeySpecException 
	     */
	    private static boolean verifySigniture(PublicKey pub, String sigFile,String cipherFile) throws IOException, SignatureException, InvalidKeyException, NoSuchAlgorithmException
	    {
	    	Signature s = Signature.getInstance(signatureType); 
			s.initVerify(pub);
			
			File sigF = new File(sigFile);
	        File message = new File(cipherFile);
	        
	        //Get signature bytes
	        FileInputStream sigStream = new FileInputStream(sigF);
	        byte[] sigBytes = new byte[(int) sigF.length()];
	        sigStream.read(sigBytes);
	        
	        //Get message bytes
	        FileInputStream msgStream = new FileInputStream(message);
	        byte[] msgBytes = new byte[(int) message.length()];
	        msgStream.read(msgBytes);
	        
	        msgStream.close();
	        sigStream.close();
	        s.update(msgBytes);
	        //Return verification on bytes
	        try
	        {
				return s.verify(sigBytes);
	        }
	        catch (Exception e)
	        {
	        	return false;
	        }
	    }
	    
	    
	    /**
	     * Create a signature for the given file
	     * @param priv: Private key for signing
	     * @param fileName signature file name to create
	     * @throws NoSuchAlgorithmException 
	     * @throws InvalidKeyException 
	     * @throws IOException 
	     * @throws SignatureException 
	     * @throws NoSuchProviderException 
	     * @param file: File to sign
	     */
	    private static void createSigniture(PrivateKey priv, String file, String fileName) throws NoSuchAlgorithmException, InvalidKeyException, IOException, SignatureException
	    {
	    	Signature s = Signature.getInstance(signatureType); 
			s.initSign(priv);
			
	        File input = new File(file);
	        File output = new File(fileName);
	        
	        //Get message bytes for signature generation
	        FileInputStream inputStream = new FileInputStream(input);
	        byte[] inputBytes = new byte[(int) input.length()];
	        inputStream.read(inputBytes);
	        
	        s.update(inputBytes);
	        byte[] sig = s.sign();
	        
	        //Write the signature
	        FileOutputStream outputStream = new FileOutputStream(output);
	        outputStream.write(sig);
	        inputStream.close();
	        outputStream.close();
	    }
	    
	    /**
	     * Get a PublicKey object from its bytes
	     * @param pub: Public key 
	     * @return Public Key
	     * @throws InvalidKeySpecException
	     * @throws NoSuchAlgorithmException
	     */
	    private static PublicKey getPublicKeyFromBytes(byte[] pub) throws InvalidKeySpecException, NoSuchAlgorithmException
	    {
	    	X509EncodedKeySpec ks = new X509EncodedKeySpec(pub);
			KeyFactory kf = KeyFactory.getInstance(asymetric);
			return kf.generatePublic(ks);
	    }
	    
	    /**
	     * Return a PrivateKey object from its bytes
	     * @param priv: Private key bytes
	     * @return PrivateKey
	     * @throws InvalidKeySpecException
	     * @throws NoSuchAlgorithmException
	     */
	    private static PrivateKey getPrivateKeyFromBytes(byte[] priv) throws InvalidKeySpecException, NoSuchAlgorithmException
	    {
	    	PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(priv);
			KeyFactory kf = KeyFactory.getInstance(asymetric);
			return kf.generatePrivate(ks);
	    }
	    
	    
	    /**
	     * Generate KeyPairs
	     * @return KeyPair
	     * @throws NoSuchAlgorithmException
	     * @throws NoSuchProviderException
	     */
	    private static KeyPair generateKeyPairs() throws NoSuchAlgorithmException, NoSuchProviderException
	    {
	    	KeyPairGenerator keyGen = KeyPairGenerator.getInstance(asymetric);
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
			keyGen.initialize(1024, random);
			return keyGen.generateKeyPair();
	    }
	    
	    
	    /**
	     * Encrypt a file
	     * @param key : Key for file encryption
	     * @param inputFile : File to encrypt
	     * @param outputFile : Cipher file 
	     * @throws Exception
	     */
	    private static void encryptFile(Key key, String inputFile, String outputFile) throws Exception
	    {
	        Cipher cipher = Cipher.getInstance(cipherAlgorithm);
	        cipher.init(Cipher.ENCRYPT_MODE, key,ivspec);
	        fileCipher(inputFile,outputFile,cipher);
	    }
	    
	    /**
	     * Perform cipher operation on file
	     * @param inputFile : Input for cipher operation
	     * @param outputFile : Output for cipher operation
	     * @param cipher : Cipher to use
	     * @throws Exception
	     */
	    private static void fileCipher(String inputFile, String outputFile,Cipher cipher) throws Exception
	    {
	        File input = new File(inputFile);
	        File output = new File(outputFile);
	        //Read in input bytes
	        FileInputStream inputStream = new FileInputStream(input);
	        byte[] inputBytes = new byte[(int) input.length()];
	        inputStream.read(inputBytes);
	        //Encrypt input bytes
	        byte[] outputBytes = cipher.doFinal(inputBytes);
	        // Write encrypted bytes
	        FileOutputStream outputStream = new FileOutputStream(output);
	        outputStream.write(outputBytes);
	        inputStream.close();
	        outputStream.close();
	    }
	    
	    /**
	     * Decrypt a file
	     * @param key : Key for decryption
	     * @param inputFile : File to decrypt
	     * @param outputFile : Plaintext file
	     * @throws Exception
	     */
	    private static void decryptFile(Key key, String inputFile, String outputFile) throws Exception
	    {
	        Cipher cipher = Cipher.getInstance(cipherAlgorithm);
	        cipher.init(Cipher.DECRYPT_MODE, key,ivspec);
	        fileCipher(inputFile,outputFile,cipher);
	    }
	    
		/**
		 * Get the Key version of a key in byte form
		 * @param key : key in bytes
		 * @return Key object
		 * @throws Exception
		 */
		private static Key getKeyFromBytes(byte[] key) throws Exception
		{
			return generateKeyFromBytes(key,encryptionAlgorithm);
		}
		
		/**
		 * Check a password against a given hash and salt
		 * @param hash : Hash bytes
		 * @param salt : salt bytes
		 * @param password : plaintext password
		 * @return
		 */
		private static boolean checkPassword(byte[] hash, byte[] salt, String password)
		{
			return Arrays.equals(hash, Crypto.hashPasswordSHA256(password, salt));
		}
		
		
		/**
		 * Generate a key for the specified encryption algorithm
		 * @return : SecretKey object
		 * @throws NoSuchAlgorithmException
		 */
		private static SecretKey generateKey() throws NoSuchAlgorithmException
		{
		    KeyGenerator keyGen = KeyGenerator.getInstance(encryptionAlgorithm);
		    return keyGen.generateKey();
		}
		
		/**
		 * Get a key from hashing the supplied plaintext
		 * @param plaintext : Plaintext password
		 * @return Key for encrypting/decrypting other keys
		 * @throws Exception
		 */
		private static Key getKeyFromHash(String plaintext) throws Exception
		{
			return generateKeyFromBytes(Crypto.hashPasswordMD5(plaintext),encryptionAlgorithm);
		}
		
		/**
		 * Encrypt a AES key 
		 * @param aeskey: aesKey for encryption
		 * @param key: Key to encrypt aes key
		 * @return encrypted aes key as hex string
		 * @throws Exception
		 */
		private static String encryptKey(SecretKey aeskey, Key key) throws Exception
		{
			Cipher aes = Cipher.getInstance(cipherAlgorithm);
			// Encrypt the files aes key using a read AES key
			aes.init(Cipher.ENCRYPT_MODE, key,ivspec);
			return getHexStr(aes.doFinal(aeskey.getEncoded())); 
		}
		
		/**
		 * Encrypt a private key
		 * @param privkey: privkey for encryption
		 * @param key: key to encrypt privkey
		 * @return encrypted private key as hex string
		 * @throws Exception
		 */
		private static String encryptKey(PrivateKey privkey, Key key) throws Exception
		{
			Cipher aes = Cipher.getInstance(cipherAlgorithm);
			// Encrypt the files aes key using a read AES key
			aes.init(Cipher.ENCRYPT_MODE, key,ivspec);
			return getHexStr(aes.doFinal(privkey.getEncoded())); 
		}
		
		
		/**
		 * Decrypt the AES key
		 * @param cipherText: Encrypted key
		 * @param key: Key for decryption
		 * @return Decrypted key in byte form
		 * @throws Exception
		 */
		private static byte[] decryptKey(String cipherText, Key key) throws Exception
		{
			Cipher aes = Cipher.getInstance(cipherAlgorithm);
			aes.init(Cipher.DECRYPT_MODE, key,ivspec);
			return aes.doFinal(hexStr2BArr(cipherText));
		}
		
		/**
		 * Generate a Key object from a byte key
		 * @param secKey : key in byte form
		 * @param algorithm : algorithm for key 
		 * @return Key object
		 * @throws Exception
		 */
		private static Key generateKeyFromBytes(final byte[] secKey, String algorithm) throws Exception 
		{
		    final Key key = new SecretKeySpec(secKey, algorithm);
		    return key;
		}
		
		
		/**
		 * Hash plaintext using SHA 256 and return hash in hexadecimal format
		 * @param plaintext : Plaintext password
		 * @param salt : Random salt in byte form
		 * @return Hash in bytes
		 */
	    private static byte[] hashPasswordSHA256(String plaintext, byte[] salt)
	    {
	    	byte[] bytes = null;
	        try 
	        {
	            MessageDigest md = MessageDigest.getInstance("SHA-256");
	            md.update(salt);
	            bytes = md.digest(plaintext.getBytes());            
	        } 
	        catch (NoSuchAlgorithmException e) 
	        {
	            System.out.println("Hash password error");
	        }
	        return bytes;
	    }
	    
		/**
		 * Hash plaintext using MD5 (128 bit) and return hash in hexadecimal format
		 * @param plaintext : Plaintext password
		 * @return hash in byte form.
		 */
	    private static byte[] hashPasswordMD5(String plaintext)
	    {
	    	byte[] bytes = null;
	        try 
	        {
	            MessageDigest md = MessageDigest.getInstance("MD5");
	            bytes = md.digest(plaintext.getBytes()); 
	        } 
	        catch (NoSuchAlgorithmException e) 
	        {
	            System.out.println("Hash password error");
	        }
	        return bytes;
	    }
	    
	     
	    /**
	     * Generate Random Salt for hashing algorithm
	     * @return Random salt in byte form
	     * @throws NoSuchAlgorithmException
	     */
	    private static byte[] getSalt() throws NoSuchAlgorithmException
	    {
	    	//Use SecureRandom for pseudo random number generator algorithm
	        SecureRandom sr = new SecureRandom();
	        byte[] salt = new byte[16];
	        sr.nextBytes(salt);
	        return salt;
	    }
	    
	    /**
	     * Get Hexadecimal string version of array of bytes
	     * @param bytes: Bytes for transformation to hex string
	     * @return Random salt in byte form
	     */
	    private static String getHexStr(byte[] bytes)
	    {
	    	StringBuilder sb = new StringBuilder();
	        // Create HEX string
	        for (byte b : bytes) 
	        {
	            sb.append(String.format("%02x", b));
	        }
	        return sb.toString();
	    	
	    }
	    
	    /**
	     * Convert a HexString into an array of bytes
	     * 
	     * Taken from: http://stackoverflow.com/questions/140131/convert-a-string-representation-of-a-hex-dump-to-a-byte-array-using-java
	     * 
	     */
	    private static byte[] hexStr2BArr(String s) 
	    {
	        int len = s.length();
	        byte[] data = new byte[len / 2];
	        for (int i = 0; i < len; i += 2) 
	        {
	            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                                 + Character.digit(s.charAt(i+1), 16));
	        }
	        return data;
	    }
	    
			
	}

	
	/**************************Files*******************************/	
	/**
	 * Class for reading/writing from/to files that contain hashed passwords, salts and encrypted keys
	 * 
	 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
 	 * Student Number: 120121386
	 */
	private static class Files 
	{
	
	    private static ArrayList<FileRecord> files = new ArrayList<FileRecord>();
		private static String pwfile =  folder + "passwords_list";
		private static String hpwFile = folder + "passwords_list_hashed";
		private static String saltFile = folder + "passwords_list_salts";
		private static String encryptedAESFile = folder + "encrypted_aes_keys";
		private static String keyPairFile = folder + "keypairs";
			
		/**
		 * Run setup - Not used in Protect normal operation
		 * @throws Exception
		 */
		private static void setup() throws Exception 
		{
			
			Files.readFiles(Files.pwfile);
			Files.makeFiles(Files.hpwFile,Files.saltFile,Files.encryptedAESFile,Files.keyPairFile);
			
		}
		
		/**
		 * Read in a specified plaintext password file
		 * @param pwfile: Plaintext password file
		 */
		private static void readFiles(String pwfile)
		{
		    Scanner sc;
			try 
			{
				sc = new Scanner(new FileReader(pwfile));
				sc.nextLine(); //Remove Header
			    while (sc.hasNextLine())
			    {
			    	String[] split= sc.nextLine().replace("\n", "").replace("\r","").split("\t");
			    	files.add(new FileRecord(split[0],split[1],split[2]));
			    }
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			}
			
		}
		
		/**
		 * Make all the hashed passwords, salt and encrypted AES files from stored plaintext passwords
		 * @param hFileName: Hashed passwords file
		 * @param sFileName : salt file
		 * @param aesFileName: encrypted aes keys file
		 * @param keyPFileName: Key pairs file
		 * @throws Exception
		 */
		private static void makeFiles(String hFileName,String sFileName,String aesFileName,String keyPFileName) throws Exception
		{
			PrintWriter hwriter; //hashed password file
			PrintWriter swriter; //salts password file
			PrintWriter aeswriter; //Encrypted AES keys file
			PrintWriter kpwriter; //Public and private keys
			try 
			{
				hwriter = new PrintWriter(hFileName, "UTF-8");
				swriter = new PrintWriter(sFileName,"UTF-8");
				aeswriter = new PrintWriter(aesFileName,"UTF-8");
				kpwriter = new PrintWriter(keyPFileName,"UTF-8");
				
				for(int i = 0; i < files.size(); i++)
				{
					FileRecord f = files.get(i);
		
					//Read in the plaintext read/write passwords for each file
					String rp = f.getReadPassword();
					String wp = f.getWritePassword();
					
					//Generate random salts for each read/write password
					byte[] rpsalt = Crypto.getSalt();
					byte[] wpsalt = Crypto.getSalt();
					
					//Generate a hash for each read/write password
					byte[] rphash = Crypto.hashPasswordSHA256(rp, rpsalt);
					byte[] wphash = Crypto.hashPasswordSHA256(wp, wpsalt);
					
					//Generate a 128 bit key from each read/write password to encrypt the files AES key
					final Key rpAESKey = Crypto.getKeyFromHash(rp);
					final Key wpAESKey = Crypto.getKeyFromHash(wp);
					
					//Generate the files AES key
					SecretKey aeskey = Crypto.generateKey();
	
					// Encrypt the files AES key using a read AES key
					String rpAESCipherText = Crypto.encryptKey(aeskey, rpAESKey);
					
					//Encrypt the files AES key using a write AES key
					String wpAESCipherText = Crypto.encryptKey(aeskey,	wpAESKey);
					
					// DSA Key pair generator
					KeyPair pair = Crypto.generateKeyPairs();
					PrivateKey priv = pair.getPrivate();
					PublicKey pub = pair.getPublic();
					
					// Encrypt the private key using a hashed version of the write password
					// Public Key does not need to be encrypted
					String privKeyCipherText = Crypto.encryptKey(priv, wpAESKey);
					
					// Write all ciphertext items to files.
					hwriter.println(String.format("%s\t%s\t%s", f.getFileName(),Crypto.getHexStr(rphash),Crypto.getHexStr(wphash)));
					swriter.println(String.format("%s\t%s\t%s", f.getFileName(),Crypto.getHexStr(rpsalt),Crypto.getHexStr(wpsalt)));
					aeswriter.println(String.format("%s\t%s\t%s", f.getFileName(),rpAESCipherText,wpAESCipherText));
					kpwriter.println(String.format("%s\t%s\t%s", f.getFileName(),Crypto.getHexStr(pub.getEncoded()),privKeyCipherText));
					
				}
				
				hwriter.close();
				swriter.close();
				aeswriter.close();
				kpwriter.close();
				
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		
		/**
		 * Return set of auxiliary files used by the program.
		 * @return set of files
		 */
		private static Set<String> getAuxFiles()
		{
			Set<String> s = new HashSet<String>();
			s.add(hpwFile);
			s.add(saltFile);
			s.add(encryptedAESFile);
			s.add(keyPairFile);
			//Stop the removal of class and java file when check is used.
			s.add("./Protect.java");
			s.add("./Protect.class");
			s.add("./Protect$Crypto.class");
			s.add("./Protect$FileKey.class");
			s.add("./Protect$FileRecord.class");
			s.add("./Protect$Files.class");
			s.add("./Protect$1.class");

			return s;
		}
		
		
		/**
		 * Read in all the hashed passwords, salts and encrypted AES keys from their associated files.
		 * @return Map of file names to their key store
		 */
		private static Map<String,FileKey> getFileKeys()
		{
		    Scanner sc;
		    Map<String,FileKey> filekeys = new HashMap<String,FileKey>();
			try 
			{
				//Read in the hashed passwords for each file
				sc = new Scanner(new FileReader(hpwFile));
			    while (sc.hasNextLine())
			    {
			    	String[] split= sc.nextLine().replace("\n", "").replace("\r","").split("\t");
			    	FileKey fk = new FileKey();
			    	fk.setFileName(split[0]);
			    	//Convert hexstrings to bytes
			    	fk.setRpHash(Crypto.hexStr2BArr(split[1]));
			    	fk.setWpHash(Crypto.hexStr2BArr(split[2]));
			    	filekeys.put(split[0], fk);
			    }
			    
			    //Read in salts
			    sc = new Scanner(new FileReader(saltFile));
			    while (sc.hasNextLine())
			    {
			    	String[] split= sc.nextLine().replace("\n", "").replace("\r","").split("\t");
			    	FileKey fk = filekeys.get(split[0]);
			    	fk.setRpSalt(Crypto.hexStr2BArr(split[1]));
			    	fk.setWpSalt(Crypto.hexStr2BArr(split[2]));
			    	filekeys.put(split[0], fk);
			    }
			    
			    //Read in encrypted AES keys
			    sc = new Scanner(new FileReader(encryptedAESFile));
			    while (sc.hasNextLine())
			    {
			    	String[] split= sc.nextLine().replace("\n", "").replace("\r","").split("\t");
			    	FileKey fk = filekeys.get(split[0]);
			    	fk.setRpAESCipherText(split[1]);
			    	fk.setWpAESCipherText(split[2]);
			    	filekeys.put(split[0], fk);
			    }
			    
			    // Read in private and public keys
			    sc = new Scanner(new FileReader(keyPairFile));
			    while(sc.hasNextLine())
			    {
			    	String[] split= sc.nextLine().replace("\n", "").replace("\r","").split("\t");
				    	FileKey fk = filekeys.get(split[0]);
				    	fk.setPublicKey(Crypto.hexStr2BArr(split[1]));
				    	fk.setPkeyCipherText(split[2]);
				    	filekeys.put(split[0], fk);
				    }
				    
				    
			} catch (FileNotFoundException e) 
			{
	            System.out.println("Read file error");
			}
				
			return filekeys;
				
			}
		}
	
	
	
	/************************ File Record ******************************/
	/**
	 * To store a filename and read/write items
	 *
	 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
	 * Student Number: 120121386
	 */
	private static class FileRecord 
	{
	
		private String fileName;
		private String readItem;
		private String writeItem;
		
		private FileRecord(String fileName,String readItem,String writeItem)
		{
			this.fileName = fileName;
			this.readItem = readItem;
			this.writeItem = writeItem;
		}
		
		@Override
		public String toString() 
		{
			return "fileName=" + fileName + ", readItem=" + readItem + ", writeItem=" + writeItem;
		}
	
		private String getFileName() 
		{
			return fileName;
		}
	
		private void setFileName(String fileName) 
		{
			this.fileName = fileName;
		}
	
		private String getReadPassword() 
		{
			return readItem;
		}
	
		private void setReadPassword(String readPassword) 
		{
			this.readItem = readPassword;
		}
	
		private String getWritePassword() 
		{
			return writeItem;
		}
	
		private void setWritePassword(String writePassword) 
		{
			this.writeItem = writePassword;
		}
			
	}

	/************************** Store keys for a file *********************************/
	/**
	 * Class to store a file record and its associated hashes, salts and encrypted keys
	 * 
	 * @author Chris Taylor <c.p.d.taylor@ncl.ac.uk>
	 * Student Number: 120121386
	 */
	private static class FileKey 
	{
	
		private String fileName;
		private byte[] rpHash;  //Hash for read password
		private byte[] wpHash;  //Hash for write password
		private byte[] rpSalt;  //Salt for read password
		private byte[] wpSalt;  //Salt for write password
		private String rpAESCipherText; //AES key encrypted using hashed read password
		private String wpAESCipherText; //AES key encrypted using hashed write password
		private byte[] publicKey;  // Unencrypted public key
		private String pkeyCipherText; //Encrypted private key using hashed write password
		
		private byte[] getPublicKey() 
		{
			return publicKey;
		}
		private void setPublicKey(byte[] publicKey) 
		{
			this.publicKey = publicKey;
		}
		private String getPkeyCipherText() 
		{
			return pkeyCipherText;
		}
		private void setPkeyCipherText(String pkeyCipherText) 
		{
			this.pkeyCipherText = pkeyCipherText;
		}
		private String getRpAESCipherText() 
		{
			return rpAESCipherText;
		}
		private void setRpAESCipherText(String rpAESCipherText) 
		{
			this.rpAESCipherText = rpAESCipherText;
		}
		private String getWpAESCipherText() 
		{
			return wpAESCipherText;
		}
		private void setWpAESCipherText(String wpAESCipherText) 
		{
			this.wpAESCipherText = wpAESCipherText;
		}
		private String getFileName() 
		{
			return fileName;
		}
		private void setFileName(String fileName) 
		{
			this.fileName = fileName;
		}
		private byte[] getRpHash() 
		{
			return rpHash;
		}
		private void setRpHash(byte[] rpHash) 
		{
			this.rpHash = rpHash;
		}
		private byte[] getWpHash() 
		{
			return wpHash;
		}
		private void setWpHash(byte[] wpHash) 
		{
			this.wpHash = wpHash;
		}
		private byte[] getRpSalt() 
		{
			return rpSalt;
		}
		private void setRpSalt(byte[] rpSalt) 
		{
			this.rpSalt = rpSalt;
		}
		private byte[] getWpSalt() 
		{
			return wpSalt;
		}
		private void setWpSalt(byte[] wpSalt) 
		{
			this.wpSalt = wpSalt;
		}
		
		
	}

	
}





	


